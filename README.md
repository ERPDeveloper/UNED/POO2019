# PARQUE DE ATRACCIONES 2018-2019

Práctica sobre el control de acceso y mantenimiento de un parque de atracciones.
Gestiona:
    - Entradas según tipo, temporada, grupo con aplicación de descuentos
    - Tipo de atracciones según características y personal de mantenimiento
    - Trabajadores: Mantenimiento y Atención al cliente
    - Estadísticas de ventas

## Pre-requisitos

Desarrollado en Java con el JDK 12, Encoding UTF-8


## Ejecutando las pruebas


Para la ejecución del programa utilizar uno de los siguiente métodos:
    - Ejecutar el método main de la clase Parque.
    - abrir una consola y ejecutar:
        `java -jar parque.jar`


## Licencia

Este proyecto está bajo la Licencia (GNU General Public License v3.0) - mira el archivo [LICENSE.md](LICENSE.md) para detalles
