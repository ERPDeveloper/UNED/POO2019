/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controller;

import attractions.AtraccionesActivas;
import attractions.Attraction;
import attractions.AttractionStatus;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import tools.MenuItem;
import tools.Utilities;

/**
 * Controller of actions with attractions
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class AttractionController extends Controller {

    /*
     * Constants for actions
     */
    private static final int ACTION_ACTIVATE = 4;
    private static final int ACTION_ADD = 1;
    private static final int ACTION_DEACTIVATE = 5;
    private static final int ACTION_DEL = 2;
    private static final int ACTION_LIST = 3;

    /*
     * Constants for menu options
     */
    private static final String MENU_TEXT_CAPTION = "Atracciones";

    private static final String MENU_TEXT_ACTIVATE = "Activar";
    private static final String MENU_TEXT_ADD = "Altas";
    private static final String MENU_TEXT_DEACTIVATE = "Desactivar";
    private static final String MENU_TEXT_DEL = "Bajas";
    private static final String MENU_TEXT_LIST = "Listar";

    /*
     * Constants for request
     */
    private static final String TXT_ATTRACTION_TYPE = "Introduce el tipo de atracción [A,B,C,D,E]";
    private static final String TXT_ATTRACTION_ID = "Introduce el identificador de la atracción";
    private static final String TXT_ATTRACTION_DEL = "Se ha eliminado la atracción";

    /*
     * Constants for error messages
     */
    private static final String ERROR_ATTRACTION_YEAR = "¡Error! El año del periodo debe ser el mismo";
    private static final String ERROR_ATTRACTION_TYPE = "¡Error! No existe el tipo de atracción";
    private static final String ERROR_ATTRACTION_NO_EXIST = "¡Error! No existe la atracción indicada";

    /**
     * List of attractions
     */
    private final ArrayList<Attraction> attractionList;

    /**
     * Attraction status control
     * Activate/deactivate attraction for days in a year
     */
    private final AtraccionesActivas attractionActive;

    /**
     * Link to statistics controller
     */
    private StatisticsController statistics;

    /**
     * Contructor
     */
    public AttractionController() {
        super();
        this.attractionActive = new AtraccionesActivas();
        this.attractionList = new ArrayList<>();
        this.statistics = null;
    }

    /**
     * Active/Deactive attraction action
     *
     * @param active
     */
    private void actionActivateAttraction(boolean active) {
        /// Request attraction id
        String id = Utilities.requestString(TXT_ATTRACTION_ID, 0);
        if (id.equals("")) {
            return;
        }

        if (this.searchAttraction(id) == null) {
            System.out.println(ERROR_ATTRACTION_NO_EXIST);
            return;
        }

        /// Request period to list
        HashMap<String, Object> period = Utilities.requestPeriod("");
        if (period == null) {
            return;
        }

        /// Change status for attraction into period
        LocalDate dateFrom = (LocalDate) period.get("from");
        LocalDate dateTo = (LocalDate) period.get("to");
        this.activateAttraction(id, dateFrom, dateTo, active);
    }

    /**
     * List attraction process.
     */
    private void actionListAttraction() {
        for (Attraction attraction : this.attractionList) {
            System.out.println(attraction.toString());
            System.out.println(attraction.descriptionMonthlyCost());
            System.out.println("");
            System.out.println("");
        }
    }

    /**
     * Remove attraction process
     */
    private void actionRemoveAttraction() {
        /// Request attraction id
        String id = Utilities.requestString(TXT_ATTRACTION_ID, 0);
        if (id.equals("")) {
            return;
        }

        /// Remove attraction data
        Iterator<Attraction> it = this.attractionList.iterator();
        while (it.hasNext()) {
            if (it.next().getId().equals(id)) {
                this.attractionActive.removeAttraction(0, id);
                it.remove();
                System.out.println(TXT_ATTRACTION_DEL);
                return;
            }
        }
        System.out.println(ERROR_ATTRACTION_NO_EXIST);
    }

    /**
     * Add attraction action.
     */
    private void actionAddAttraction() {
        String type = Utilities.requestString(TXT_ATTRACTION_TYPE, 1);
        if (type.equals("")) {
            return;
        }

        this.addAttraction(type);
    }

    /**
     * Activate or deactivate an attraction on a date period of the same year
     *
     * @param id
     * @param dateFrom
     * @param dateTo
     * @param active
     */
    public void activateAttraction(String id, LocalDate dateFrom, LocalDate dateTo, boolean active) {
        if (dateFrom.getYear() != dateTo.getYear()) {
            System.out.println(ERROR_ATTRACTION_YEAR);
            return;
        }

        this.attractionActive.changeAttractionStatus(id, dateFrom, dateTo, active);
    }

    /**
     * Add attraction process.
     *
     * @param type
     * @return Attraction
     */
    public Attraction addAttraction(String type) {
        int year = LocalDate.now().getYear();
        try {
            /// Get complete class name and identificator for new instance
            String className = Attraction.class.getName() + "Type" + type.toUpperCase();
            String idAttraction = Integer.toString(this.attractionList.size() + 1);

            /// Create new instance from class name
            Class attractionClass = Class.forName(className);
            Attraction attraction = (Attraction) attractionClass
                    .getConstructor(String.class)
                    .newInstance(idAttraction);

            /// Add atraction to attraction list and status control
            this.attractionList.add(attraction);
            this.attractionActive.addAttraction(year, attraction.getId());
            return attraction;
        } catch (InvocationTargetException
                | IllegalAccessException
                | NoSuchMethodException
                | InstantiationException
                | ClassNotFoundException exception) {
            System.out.println(ERROR_ATTRACTION_TYPE);
            return null;
        }
    }

    /**
     * Add a adult to ride attraction count
     *
     * @param date
     * @param attraction
     * @param height
     */
    public void addRideAdultToAttraction(LocalDate date, Attraction attraction, int height) {
        AttractionStatus status = this.attractionActive.getAttractionStatus(date.getYear(), attraction.getId());
        boolean active = status.getStatus(date);

        if (active && attraction.check(height, false)) {
            this.statistics.addRideAdultToAttraction(date, attraction, status, 1);
        }
    }

    /**
     * Add a children to ride attraction count
     *
     * @param date
     * @param attraction
     * @param height
     */
    public void addRideChildrenToAttraction(LocalDate date, Attraction attraction, int height) {
        AttractionStatus status = this.attractionActive.getAttractionStatus(date.getYear(), attraction.getId());
        boolean active = status.getStatus(date);

        if (active && attraction.check(height, true)) {
            this.statistics.addRideChildrenToAttraction(date, attraction, status, 1);
        }
    }

    /**
     * Execute the menu option indicated.
     * Menu continue until exit option execute or false return
     *
     * @param menuItem
     * @return boolean
     */
    @Override
    public boolean execOption(MenuItem menuItem) {
        int option = menuItem.getOption();
        switch (option) {
            case ACTION_ADD:
                this.actionAddAttraction();
                break;

            case ACTION_DEL:
                this.actionRemoveAttraction();
                break;

            case ACTION_LIST:
                this.actionListAttraction();
                break;

            case ACTION_ACTIVATE:
            case ACTION_DEACTIVATE:
                this.actionActivateAttraction(option == ACTION_ACTIVATE);
                break;

        }
        return true;
    }

    /**
     * Obtain attraction list
     *
     * @return ArrayList
     */
    public ArrayList<Attraction> getAttractionList() {
        return attractionList;
    }

    /**
     * Gets the list of options for the preparation of the menu.
     *
     * @return MenuItem[]
     */
    @Override
    public MenuItem getMenu() {
        MenuItem item = new MenuItem(MENU_TEXT_CAPTION, 0, this);
        item.addChildren(MENU_TEXT_ADD, ACTION_ADD);
        item.addChildren(MENU_TEXT_ACTIVATE, ACTION_ACTIVATE);
        item.addChildren(MENU_TEXT_DEACTIVATE, ACTION_DEACTIVATE);
        item.addChildren(MENU_TEXT_LIST, ACTION_LIST);
        item.addChildren(MENU_TEXT_DEL, ACTION_DEL);
        return item;
    }

    /**
     * Get the attraction with the indicated identifier
     *
     * @param id
     * @return Attraction
     */
    private Attraction searchAttraction(String id) {
        for (Attraction attraction : this.attractionList) {
            if (attraction.getId().equals(id)) {
                return attraction;
            }
        }
        return null;
    }

    /**
     * Establishes the link with the statistics controller
     *
     * @param statistics
     */
    public void setStatistics(StatisticsController statistics) {
        this.statistics = statistics;
    }
}
