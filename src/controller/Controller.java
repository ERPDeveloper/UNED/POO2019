/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controller;

import tools.Menu;
import tools.MenuItem;

/**
 * Theme park application.
 * Base class for application controllers.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public abstract class Controller {

    /**
     * Contructor
     */
    public Controller() {
    }

    /**
     * Show and execute controller menu.
     */
    public void execMenu() {
        Menu menu = new Menu();
        MenuItem menuItem = this.getMenu();
        menu.addItem(menuItem);
        menu.execute(menuItem.getCaption(), menuItem.getChildrenList());
    }

    /**
     * Execute the menu option indicated.
     * Menu continue until exit option execute or false return
     *
     * @param menuItem
     * @return boolean
     */
    public abstract boolean execOption(MenuItem menuItem);

    /**
     * Gets the list of options for the preparation of the menu.
     *
     * @return MenuItem[]
     */
    public abstract MenuItem getMenu();

    /**
     * Returns an optional caption for an item menu
     *
     * @param menuItem
     * @return String
     */
    public String getOptionCaption(MenuItem menuItem) {
        return "";
    }
}
