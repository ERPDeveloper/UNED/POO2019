/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controller;

import java.time.LocalDate;
import ticket.Person;
import ticket.Ticket;
import ticket.TicketGeneral;
import tools.MenuItem;
import tools.Utilities;

/**
 * Controller of actions for new issuance of tickets
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class TicketAddController extends Controller {

    /*
     * Constants for actions
     */
    private static final int ACTION_ADDPERSON = 3;
    private static final int ACTION_CONFIRM = 7;
    private static final int ACTION_DATE = 1;
    private static final int ACTION_DELPERSON = 5;
    private static final int ACTION_LIST = 6;
    private static final int ACTION_MODPERSON = 4;
    private static final int ACTION_TYPE = 2;

    /*
     * Constants for menu options
     */
    private static final String MENU_TEXT_CAPTION = "Altas de Entradas";

    private static final String MENU_TEXT_ADDPERSON = "Añadir persona";
    private static final String MENU_TEXT_CONFIRM = "Confirmar tíquet";
    private static final String MENU_TEXT_DATE = "Establecer fecha";
    private static final String MENU_TEXT_DELPERSON = "Quitar persona";
    private static final String MENU_TEXT_EXIT = "Cancelar tíquet";
    private static final String MENU_TEXT_LIST = "Ver tíquet";
    private static final String MENU_TEXT_MODPERSON = "Modificar persona";
    private static final String MENU_TEXT_TYPE = "Establecer tipo";

    /*
     * Constants for request
     */
    private static final String TEXT_NEW_AGE = "Introducir nueva edad:";
    private static final String TEXT_NEW_DATE = "Nueva fecha:";
    private static final String TEXT_NEW_HEIGHT = "Introducir nueva altura:";
    private static final String TEXT_SELECT_PERSON = "Seleccione persona:";

    /*
     * Constants for error messages
     */
    private static final String ERROR_NO_ADD_PERSON = "¡Error! No ha sido posible añadir la persona";
    private static final String ERROR_NO_DATE = "¡Error! No se puede asignar la fecha indicada";

    /**
     * Indicates if the ticket is confirmed and posted
     */
    private boolean confirmed;

    /**
     * Structure with the data of the entrance of the group of people.
     * It contains all the special features contracted and the final price of it.
     */
    private Ticket ticket;

    /**
     * Contructor
     */
    public TicketAddController() {
        super();

        this.confirmed = false;
        this.ticket = new TicketGeneral();
    }

    /**
     * Add a new person according to the age and height entered
     */
    private void actionAddPerson() {
        int age = Utilities.requestInt(TEXT_NEW_AGE, 0, 99);
        if (age == 0) {
            return;
        }

        int height = Utilities.requestInt(TEXT_NEW_HEIGHT, 20, 250);
        if (height == 0) {
            return;
        }

        int personIndex = this.ticket.addPerson(age, height);
        if (personIndex < 0) {
            System.out.println(ERROR_NO_ADD_PERSON);
            return;
        }
        this.modifyPerson(personIndex);
    }

    /**
     * Mark the ticket as correct and accepted
     *
     * @return boolean
     */
    private boolean actionConfirm() {
        this.confirmed = this.ticket.check(true);
        return !this.confirmed;
    }

    /**
     * List group of persons
     */
    private void actionList() {
        System.out.println(this.ticket.toString());
        System.out.println("-----------------------------------");
        this.listPersons();
    }

    /**
     * List and request to user that person to modify
     */
    private void actionModifyPerson() {
        int personIndex = 1;
        if (this.ticket.getPersonList().size() > 1) {
            personIndex = this.requestPerson();
        }

        if (personIndex > 0) {
            this.modifyPerson(personIndex - 1);
        }
    }

    /**
     * Remove the person indicated by the user from the list
     */
    private void actionRemovePerson() {
        int personIndex = this.requestPerson();
        if (personIndex > 0) {
            this.ticket.removePerson(personIndex - 1);
        }
    }

    /**
     * Request a date from the user to update the ticket
     * Only updated if the date is equal or greater than current date
     */
    private void actionRequestDate() {
        LocalDate date = Utilities.requestDate(TEXT_NEW_DATE);
        if (date != null) {
            if (date.compareTo(LocalDate.now()) < 0) {
                System.out.println(ERROR_NO_DATE);
                return;
            }
            this.ticket.setDate(date);
        }
    }

    /**
     * Request a basic ticket type
     */
    private void actionRequestType() {
        /// Create controller and show options menu
        TicketTypeController controller = new TicketTypeController();
        controller.execMenu();

        // Assign new ticket if it changed
        Ticket newTicket = controller.getTicket();
        if (newTicket != null) {
            if (newTicket.getClass() != this.ticket.getClass()) {
                newTicket.copyFromTicket(this.ticket);
                this.ticket = newTicket;
            }
        }
    }

    /**
     * Execute the menu option indicated.
     * Menu continue until exit option execute or false return
     *
     * @param menuItem
     * @return boolean
     */
    @Override
    public boolean execOption(MenuItem menuItem) {
        switch (menuItem.getOption()) {
            case ACTION_ADDPERSON:
                this.actionAddPerson();
                break;

            case ACTION_CONFIRM:
                return this.actionConfirm();

            case ACTION_DATE:
                this.actionRequestDate();
                break;

            case ACTION_DELPERSON:
                this.actionRemovePerson();
                break;

            case ACTION_LIST:
                this.actionList();
                break;

            case ACTION_MODPERSON:
                this.actionModifyPerson();
                break;

            case ACTION_TYPE:
                this.actionRequestType();
                break;
        }
        return true;
    }

    /**
     * Gets the list of options for the preparation of the menu.
     *
     * @return MenuItem[]
     */
    @Override
    public MenuItem getMenu() {
        MenuItem item = new MenuItem(MENU_TEXT_CAPTION, 0, this);
        item.addChildren(MENU_TEXT_DATE, ACTION_DATE);
        item.addChildren(MENU_TEXT_TYPE, ACTION_TYPE);
        item.addChildren(MENU_TEXT_ADDPERSON, ACTION_ADDPERSON);
        item.addChildren(MENU_TEXT_MODPERSON, ACTION_MODPERSON);
        item.addChildren(MENU_TEXT_DELPERSON, ACTION_DELPERSON);
        item.addChildren(MENU_TEXT_LIST, ACTION_LIST, true);
        item.addChildren(MENU_TEXT_CONFIRM, ACTION_CONFIRM);
        item.addChildren(MENU_TEXT_EXIT, MenuItem.MENU_ITEM_CUSTOMEXIT);
        return item;
    }

    /**
     * Returns an optional caption for an item menu
     *
     * @param menuItem
     * @return String
     */
    @Override
    public String getOptionCaption(MenuItem menuItem) {
        switch (menuItem.getOption()) {
            case ACTION_DATE:
                return Utilities.dateToString(this.ticket.getDate());

            case ACTION_TYPE:
                return this.ticket.getDescription() + " (" + this.ticket.descriptionSeason() + ")";

            case ACTION_ADDPERSON:
                return this.ticket.descriptionPerson();

            case ACTION_CONFIRM:
                return this.ticket.descriptionPrice();
        }
        return super.getOptionCaption(menuItem);
    }

    /**
     * Get structure with the data of the entrance of the group of people
     *
     * @return Ticket
     */
    public Ticket getTicket() {
        return this.ticket;
    }

    /**
     * It allows to know if the ticket is confirmed and published.
     *
     * @return boolean
     */
    public boolean isConfirmed() {
        return this.confirmed;
    }

    /**
     * Displays the list of people on the screen
     */
    private void listPersons() {
        int index = 0;
        for (Person person : this.ticket.getPersonList()) {
            index++;
            System.out.println(Integer.toString(index) + " - " + person.toString());
        }
    }

    /**
     * Modify the characteristics of the person indicated
     *
     * @param personIndex
     */
    private void modifyPerson(int personIndex) {
        /// Create controller and show options menu
        Person person = this.ticket.getPerson(personIndex);
        PersonController controller = new PersonController(person);
        controller.execMenu();
        this.ticket.recalculatePersons();
    }

    /**
     * Display the list of people and request a user
     *
     * @return int
     */
    private int requestPerson() {
        this.listPersons();
        return Utilities.requestInt(TEXT_SELECT_PERSON, 0, this.ticket.getPersonList().size());
    }
}
