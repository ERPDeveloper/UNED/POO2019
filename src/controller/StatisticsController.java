/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controller;

import attractions.Attraction;
import attractions.AttractionStatus;
import java.time.LocalDate;
import statistics.AssistanceReport;
import statistics.AssistanceStatistics;
import statistics.RideAttractionReport;
import statistics.RideAttractionStatistics;
import ticket.Ticket;
import tools.MenuItem;

/**
 * Controller of the statistics of the application
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class StatisticsController extends Controller {

    /*
     * Constants for actions
     */
    private static final int ACTION_ATTRACTION_ANNUAL = 110;
    private static final int ACTION_ATTRACTION_BY_DATE = 130;
    private static final int ACTION_ATTRACTION_MONTHLY = 120;
    private static final int ACTION_ASSISTANCE_ANNUAL = 210;
    private static final int ACTION_ASSISTANCE_BY_DATE = 230;
    private static final int ACTION_ASSISTANCE_MONTHLY = 220;

    /*
     * Constants for menu options
     */
    private static final String MENU_TEXT_CAPTION = "Estadísticas";

    private static final String MENU_TEXT_ANNUAL = "Anual";
    private static final String MENU_TEXT_ASSISTANCE = "Asistencias";
    private static final String MENU_TEXT_ATTRACTION = "Atracciones";
    private static final String MENU_TEXT_BY_DATE = "Por Fecha";
    private static final String MENU_TEXT_MONTHLY = "Mensual";

    /**
     * Assistance statistics
     */
    private final AssistanceStatistics assistance;

    /**
     * Ride Attraction statistics
     */
    private final RideAttractionStatistics rideAttraction;

    /**
     * Constructor
     */
    public StatisticsController() {
        super();
        this.assistance = new AssistanceStatistics();
        this.rideAttraction = new RideAttractionStatistics();
    }

    /**
     * Accumulate quantities and amounts into assistance statistic
     *
     * @param ticket
     */
    public void accumulateTicket(Ticket ticket) {
        LocalDate date = ticket.getDate();
        int[] quantities = ticket.quantityByType();
        double[] amounts = ticket.totalByType();

        this.assistance.accumulateIntoYear(date, quantities, amounts);
    }

    /**
     * Displays ride attraction data for one year
     */
    private void actionRideAttractionAnnual() {
        RideAttractionReport report = new RideAttractionReport(this.rideAttraction);
        report.annual();
    }

    /**
     * Displays ride attraction data between a period of dates
     */
    private void actionRideAttractionByDate() {
        RideAttractionReport report = new RideAttractionReport(this.rideAttraction);
        report.byDate();
    }

    /**
     * Displays ride attraction data for one month
     */
    private void actionRideAttractionMonth() {
        RideAttractionReport report = new RideAttractionReport(this.rideAttraction);
        report.monthly();
    }

    /**
     * Displays attendance data for one year
     */
    private void actionAssistanceAnnual() {
        AssistanceReport report = new AssistanceReport(this.assistance);
        report.annual();
    }

    /**
     * Displays attendance data between a period of dates
     */
    private void actionAssistanceByDate() {
        AssistanceReport report = new AssistanceReport(this.assistance);
        report.byDate();
    }

    /**
     * Displays attendance data for one month
     */
    private void actionAssistanceMonth() {
        AssistanceReport report = new AssistanceReport(this.assistance);
        report.monthly();
    }

    /**
     * Add the quantity of Adult to ride attraction count
     *
     * @param date
     * @param attraction
     * @param status
     * @param quantity
     */
    public void addRideAdultToAttraction(LocalDate date, Attraction attraction, AttractionStatus status, int quantity) {
        /// Create quantities array: Adults, Children
        int[] quantities = {quantity, 0};

        /// Acumulate into adult
        this.rideAttraction.accumulateIntoYear(date, attraction, status, quantities);
    }

    /**
     * Add the quantity of Children to ride attraction count
     *
     * @param date
     * @param attraction
     * @param status
     * @param quantity
     */
    public void addRideChildrenToAttraction(LocalDate date, Attraction attraction, AttractionStatus status, int quantity) {
        /// Create quantities array: Adults, Children
        int[] quantities = {0, quantity};

        /// Acumulate into infant
        this.rideAttraction.accumulateIntoYear(date, attraction, status, quantities);
    }

    /**
     * Desacumulate quantities and amounts into assistance statistic
     *
     * @param ticket
     */
    public void desacumulateTicket(Ticket ticket) {
        LocalDate date = ticket.getDate();
        int[] quantities = ticket.quantityByType();
        double[] amounts = ticket.totalByType();

        this.assistance.desacumulateIntoYear(date, quantities, amounts);
    }

    /**
     * Execute the menu option indicated.
     * Menu continue until exit option execute or false return
     *
     * @param menuItem
     * @return boolean
     */
    @Override
    public boolean execOption(MenuItem menuItem) {
        switch (menuItem.getOption()) {
            case ACTION_ATTRACTION_ANNUAL:
                this.actionRideAttractionAnnual();
                break;

            case ACTION_ATTRACTION_BY_DATE:
                this.actionRideAttractionByDate();
                break;

            case ACTION_ATTRACTION_MONTHLY:
                this.actionRideAttractionMonth();
                break;

            case ACTION_ASSISTANCE_ANNUAL:
                this.actionAssistanceAnnual();
                break;

            case ACTION_ASSISTANCE_BY_DATE:
                this.actionAssistanceByDate();
                break;

            case ACTION_ASSISTANCE_MONTHLY:
                this.actionAssistanceMonth();
                break;
        }
        return true;
    }

    /**
     * Gets the list of options for the preparation of the menu.
     *
     * @return MenuItem[]
     */
    @Override
    public MenuItem getMenu() {
        /// Create Main Menu
        MenuItem menuMain = new MenuItem(MENU_TEXT_CAPTION, 0, this);

        /// Create Attraction Sub-Menu
        MenuItem menuAttraction = menuMain.addChildren(MENU_TEXT_ATTRACTION, 10);
        menuAttraction.addChildren(MENU_TEXT_ANNUAL, 110);
        menuAttraction.addChildren(MENU_TEXT_MONTHLY, 120);
        menuAttraction.addChildren(MENU_TEXT_BY_DATE, 130);

        /// Create Assistance Sub-Menu
        MenuItem menuAssistance = menuMain.addChildren(MENU_TEXT_ASSISTANCE, 20);
        menuAssistance.addChildren(MENU_TEXT_ANNUAL, 210, true);
        menuAssistance.addChildren(MENU_TEXT_MONTHLY, 220);
        menuAssistance.addChildren(MENU_TEXT_BY_DATE, 230);

        /// Return Menu
        return menuMain;
    }
}
