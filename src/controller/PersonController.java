/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controller;

import java.util.ArrayList;
import ticket.DiscountEnum;
import ticket.Person;
import tools.MenuItem;
import tools.Utilities;

/**
 * Controller actions for an person ticket
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class PersonController extends Controller {

    /*
     * Constants for actions
     */
    private static final int ACTION_ADD = 4;
    private static final int ACTION_AGE = 1;
    private static final int ACTION_DEL = 5;
    private static final int ACTION_HEIGHT = 2;
    private static final int ACTION_VIP = 3;

    /*
     * Constants for menu options
     */
    private static final String MENU_TEXT_CAPTION = "Modificación de persona";
    private static final String MENU_TEXT_ADD = "Añadir descuento";
    private static final String MENU_TEXT_AGE = "Modificar edad";
    private static final String MENU_TEXT_DEL = "Eliminar descuento";
    private static final String MENU_TEXT_HEIGHT = "Modificar altura";
    private static final String MENU_TEXT_VIP = "Modificar VIP";

    /*
     * Constants for request
     */
    private static final String TEXT_NEW_AGE = "Introducir nueva edad:";
    private static final String TEXT_NEW_HEIGHT = "Introducir nueva altura:";
    private static final String TEXT_SELECT_DISCOUNT = "Seleccionar descuento:";

    /*
     * Constans
     */
    private static final String TXT_SUPPLEMENT_VIP = "Suplemento";

    /**
     * Person on whom changes are made
     */
    private final Person person;

    /**
     * Constructor
     *
     * @param person
     */
    public PersonController(Person person) {
        super();
        this.person = person;
    }

    /**
     * Add a new discount to the person
     */
    private void actionAddDiscount() {
        int index = 0;
        ArrayList<DiscountEnum> discounts = DiscountEnum.discountList();
        for (DiscountEnum discount : discounts) {
            index++;
            System.out.println(Integer.toString(index) + " - " + discount.toString());
        }

        index = Utilities.requestInt(TEXT_SELECT_DISCOUNT, 0, index);
        if (index > 0) {
            this.person.addDiscount(discounts.get(index - 1));
        }
    }

    /**
     * Modify the age (and type) of a person
     */
    private void actionModifyAge() {
        int age = Utilities.requestInt(TEXT_NEW_AGE, 0, 99);
        if (age > 0) {
            this.person.setAge(age);
        }
    }

    /**
     * Modify the height of a person
     */
    private void actionModifyHeight() {
        int height = Utilities.requestInt(TEXT_NEW_HEIGHT, 20, 250);
        if (height > 0) {
            this.person.setHeight(height);
        }
    }

    /**
     * Change VIP supplement status
     */
    private void actionModifySupplement() {
        this.person.setVipSupplement(!this.person.isVipSupplement());
    }

    /**
     * Remove a discount from a person
     */
    private void actionRemoveDiscount() {
        // Get actual discount list
        ArrayList<DiscountEnum> discounts = this.person.getDiscounts();
        if (discounts.isEmpty()) {
            return;
        }

        // If there is more than one discount in the list. Ask what to eliminate
        int index = 1;
        if (discounts.size() > 1) {
            for (DiscountEnum discount : discounts) {
                System.out.println(Integer.toString(index) + " - " + discount.toString());
                index++;
            }

            index = Utilities.requestInt(TEXT_SELECT_DISCOUNT, 0, index);
        }

        // If a discount has been selected we eliminate it
        if (index > 0) {
            this.person.removeDiscount(index - 1);
        }
    }

    /**
     * Execute the menu option indicated.
     * Menu continue until exit option execute or false return
     *
     * @param menuItem
     * @return boolean
     */
    @Override
    public boolean execOption(MenuItem menuItem) {
        switch (menuItem.getOption()) {
            case ACTION_ADD:
                this.actionAddDiscount();
                break;

            case ACTION_AGE:
                this.actionModifyAge();
                break;

            case ACTION_DEL:
                this.actionRemoveDiscount();
                break;

            case ACTION_HEIGHT:
                this.actionModifyHeight();
                break;

            case ACTION_VIP:
                this.actionModifySupplement();
                break;
        }

        return true;
    }

    @Override
    public String getOptionCaption(MenuItem menuItem) {
        switch (menuItem.getOption()) {
            case ACTION_ADD:
                return this.person.descriptionDescounts();

            case ACTION_AGE:
                return this.person.descriptionAge();

            case ACTION_HEIGHT:
                return this.person.descriptionHeight();

            case ACTION_VIP:
                return TXT_SUPPLEMENT_VIP + ": " + this.person.descriptionVipSupplement();
        }
        return super.getOptionCaption(menuItem);
    }

    /**
     * Gets the list of options for the preparation of the menu.
     *
     * @return MenuItem[]
     */
    @Override
    public MenuItem getMenu() {
        MenuItem item = new MenuItem(MENU_TEXT_CAPTION, 0, this);
        item.addChildren(MENU_TEXT_AGE, ACTION_AGE);
        item.addChildren(MENU_TEXT_AGE, ACTION_HEIGHT);
        item.addChildren(MENU_TEXT_VIP, ACTION_VIP);
        item.addChildren(MENU_TEXT_ADD, ACTION_ADD);
        item.addChildren(MENU_TEXT_DEL, ACTION_DEL);
        return item;
    }
}
