/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controller;

import java.lang.reflect.InvocationTargetException;
import ticket.Ticket;
import ticket.TicketGeneral;
import tools.MenuItem;
import tools.Utilities;

/**
 * Controller for request and change basic ticket type
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class TicketTypeController extends Controller {

    /**
     * Constants for tickets type
     */
    public static final String TICKET_GENERAL = "General";
    public static final String TICKET_LABORABLE = "Laborable";
    public static final String TICKET_AFTERNOON = "Afternoon";
    public static final String TICKET_FAMILY = "Family";

    /**
     * Constants for actions
     */
    private static final int ACTION_GENERAL = 1;
    private static final int ACTION_LABORABLE = 2;
    private static final int ACTION_FAMILY = 3;
    private static final int ACTION_AFTERNOON = 4;

    /*
     * Constants for menu options
     */
    private static final String MENU_TEXT_CAPTION = "Tipos de Entradas";
    private static final String MENU_TEXT_GENERAL = "General";
    private static final String MENU_TEXT_LABORABLE = "Laborable";
    private static final String MENU_TEXT_FAMILY = "Familiar";
    private static final String MENU_TEXT_AFTERNOON = "Sólo Tarde";

    /*
     * Constants for error messages
     */
    private static final String ERROR_TICKET_TYPE = "Error. No existe el tipo de ticket";

    /**
     * Structure with the data of the entrance of the group of people.
     * It contains all the special features contracted and the final price of it.
     */
    private Ticket ticket;

    /**
     * Constructor
     */
    public TicketTypeController() {
        super();

        this.ticket = null;
    }

    /**
     * Execute the menu option indicated.
     * Menu continue until exit option execute or false return
     *
     * @param menuItem
     * @return boolean
     */
    @Override
    public boolean execOption(MenuItem menuItem) {
        switch (menuItem.getOption()) {
            case ACTION_AFTERNOON:
                this.ticket = this.newTicketFromType(TICKET_AFTERNOON);
                break;

            case ACTION_FAMILY:
                this.ticket = this.newTicketFromType(TICKET_FAMILY);
                break;

            case ACTION_GENERAL:
                this.ticket = this.newTicketFromType(TICKET_GENERAL);
                break;

            case ACTION_LABORABLE:
                this.ticket = this.newTicketFromType(TICKET_LABORABLE);
                break;
        }
        return false;
    }

    /**
     * Return new intance of ticket type indicated.
     *
     * @param type
     * @return Ticket
     */
    public Ticket newTicketFromType(String type) {
        try {
            /// Get complete class name for new instance
            String className = Ticket.class.getName() + Utilities.ucFirst(type);

            /// Create new instance from class name
            Class ticketClass = Class.forName(className);
            Ticket newTicket = (Ticket) ticketClass
                    .getConstructor()
                    .newInstance();

            /// Return new instance
            return newTicket;
        } catch (InvocationTargetException
                | IllegalAccessException
                | NoSuchMethodException
                | InstantiationException
                | ClassNotFoundException exception) {
            System.out.println(ERROR_TICKET_TYPE + ": " + type);
        }

        /// If something has failed return a general ticket
        return new TicketGeneral();
    }


    /**
     * Gets the list of options for the preparation of the menu.
     *
     * @return MenuItem[]
     */
    @Override
    public MenuItem getMenu() {
        MenuItem item = new MenuItem(MENU_TEXT_CAPTION, 0, this);
        item.addChildren(MENU_TEXT_GENERAL, ACTION_GENERAL);
        item.addChildren(MENU_TEXT_LABORABLE, ACTION_LABORABLE);
        item.addChildren(MENU_TEXT_FAMILY, ACTION_FAMILY);
        item.addChildren(MENU_TEXT_AFTERNOON, ACTION_AFTERNOON);
        return item;
    }

    /**
     * Structure with the data of the entrance of the group of people.
     * It contains all the special features contracted and the final price of it.
     *
     * @return Ticket
     */
    public Ticket getTicket() {
        return this.ticket;
    }
}
