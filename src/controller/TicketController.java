/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import ticket.Ticket;
import tools.MenuItem;
import tools.Utilities;

/**
 * Controller of actions for the issuance of tickets to the park
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class TicketController extends Controller {

    /**
     * Constants for actions
     */
    private static final int ACTION_ADD = 1;
    private static final int ACTION_DEL = 2;
    private static final int ACTION_LIST = 3;

    /*
     * Constants for menu options
     */
    private static final String MENU_TEXT_CAPTION = "Taquilla";

    private static final String MENU_TEXT_ADD = "Nuevo Tíquet";
    private static final String MENU_TEXT_DEL = "Eliminar Tíquet";
    private static final String MENU_TEXT_LIST = "Consultar Tíquet";

    /*
     * Constants for request
     */
    private static final String TEXT_DATE = "Introducir fecha:";
    private static final String TEXT_SELECT_TICKET = "Seleccionar ticket:";
    private static final String TXT_TICKET_DEL = "Se ha eliminado el tíquet";

    /*
     * Constants for error messages
     */
    private static final String ERROR_NO_TICKETS = "¡Error! No hay tickets definidos";
    private static final String ERROR_TICKET_NO_EXIST = "¡Error! No existe el tíquet indicado";

    /**
     * Link to statistics controller
     */
    private StatisticsController statistics;

    /**
     * List of issuance tickets
     */
    private final ArrayList<Ticket> ticketList;

    /**
     * Controller
     */
    public TicketController() {
        super();
        this.statistics = null;
        this.ticketList = new ArrayList<>();
    }

    /**
     * Initialize and launch the ticket purchase process
     */
    private void actionAddTicket() {
        // Create new ticket and controller for configure it
        TicketAddController controller = new TicketAddController();
        controller.execMenu();

        // if confirm new ticket add to ticket list
        if (controller.isConfirmed()) {
            this.addTicket(controller.getTicket());
        }
    }

    /**
     * Search and view a ticket
     */
    private void actionList() {
        /// Check there are tickets
        if (this.ticketList.isEmpty()) {
            System.out.println(ERROR_NO_TICKETS);
            System.out.println("");
            return;
        }

        /// Request date to user
        LocalDate date = Utilities.requestDate(TEXT_DATE);
        if (date == null) {
            return;
        }

        /// Set a list of selected tickets and show ids
        for (Ticket ticket : this.ticketList) {
            if (date.compareTo(ticket.getDate()) == 0) {
                System.out.println(ticket.toString());
            }
        }
    }

    /**
     * Remove a ticket
     */
    private void actionRemoveTicket() {
        /// Request ticket to user
        int id = Utilities.requestInt(TEXT_SELECT_TICKET, 0, 0);
        if (id < 1) {
            return;
        }

        /// Remove attraction data
        Iterator<Ticket> it = this.ticketList.iterator();
        while (it.hasNext()) {
            Ticket ticket = it.next();
            if (ticket.getId() == id) {
                /// Deacumulate ticket from statistic
                this.statistics.desacumulateTicket(ticket);

                /// Remove data
                it.remove();
                System.out.println(TXT_TICKET_DEL);
                return;
            }
        }
        System.out.println(ERROR_TICKET_NO_EXIST);
    }

    /**
     * Add and counted in the statistics a new ticket
     *
     * @param ticket
     */
    public void addTicket(Ticket ticket) {
        /// Add ticket to ticket list
        this.ticketList.add(ticket);

        /// Acumulate ticket into statistic
        this.statistics.accumulateTicket(ticket);
    }

    /**
     * Execute the menu option indicated.
     * Menu continue until exit option execute or false return
     *
     * @param menuItem
     * @return boolean
     */
    @Override
    public boolean execOption(MenuItem menuItem) {
        switch (menuItem.getOption()) {
            case ACTION_ADD:
                this.actionAddTicket();
                break;

            case ACTION_DEL:
                this.actionRemoveTicket();
                break;

            case ACTION_LIST:
                this.actionList();
                break;
        }

        return true;
    }

    /**
     * Gets the list of options for the preparation of the menu.
     *
     * @return MenuItem[]
     */
    @Override
    public MenuItem getMenu() {
        MenuItem item = new MenuItem(MENU_TEXT_CAPTION, 0, this);
        item.addChildren(MENU_TEXT_ADD, ACTION_ADD);
        item.addChildren(MENU_TEXT_LIST, ACTION_LIST);
        item.addChildren(MENU_TEXT_DEL, ACTION_DEL);
        return item;
    }

    /**
     * Establishes the link with the statistics controller
     *
     * @param statistics
     */
    public void setStatistics(StatisticsController statistics) {
        this.statistics = statistics;
    }
}