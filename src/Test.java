/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import attractions.Attraction;
import controller.AttractionController;
import controller.TicketController;
import controller.TicketTypeController;
import java.time.LocalDate;
import ticket.Person;
import ticket.PersonEnum;
import ticket.Ticket;
import tools.Utilities;

/**
 * Theme park application. Test Class.
 * Management of attendance, attractions and staff.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class Test {

    private static final int ATTRACTION_MAX = 7;
    private static final int ATTRACTION_TYPE_A = 4;
    private static final int ATTRACTION_TYPE_B = 6;
    private static final int ATTRACTION_TYPE_C = 4;
    private static final int ATTRACTION_TYPE_D = 3;
    private static final int ATTRACTION_TYPE_E = 7;
    private static final int ATTRACTION_MAX_DEACTIVATE = 50;

    private static final int TEST_YEAR = 2019;

    private static final int TICKET_AFTERNOON = 1400;
    private static final int TICKET_FAMILY = 1500;
    private static final int TICKET_GENERAL = 10000;
    private static final int TICKECT_LABORABLE = 1800;
    private static final int TICKET_MAX_ADULTS = 4;
    private static final int TICKET_MAX_CHILDREN = 3;

    public Test() {
    }

    /**
     * Add Attractions and deactivate for ATTRACTION_MAX_DEACTIVATE days
     *
     * @param controller
     */
    public void addAttractions(AttractionController controller) {
        for (int i = 1; i <= ATTRACTION_MAX; i++) {
            if (i <= ATTRACTION_TYPE_A) {
                Attraction attraction = controller.addAttraction("A");
                this.deactivateAttraction(controller, attraction);
            }
            if (i <= ATTRACTION_TYPE_B) {
                Attraction attraction = controller.addAttraction("B");
                this.deactivateAttraction(controller, attraction);
            }
            if (i <= ATTRACTION_TYPE_C) {
                Attraction attraction = controller.addAttraction("C");
                this.deactivateAttraction(controller, attraction);
            }
            if (i <= ATTRACTION_TYPE_D) {
                Attraction attraction = controller.addAttraction("D");
                this.deactivateAttraction(controller, attraction);
            }
            if (i <= ATTRACTION_TYPE_E) {
                Attraction attraction = controller.addAttraction("E");
                this.deactivateAttraction(controller, attraction);
            }
        }
    }

    /**
     * Add to ride attraction statistics all ticket persons
     *
     * @param ticket
     * @param attractionController
     */
    private void addRideAttraction(Ticket ticket, AttractionController attractionController) {
        for (Attraction attraction : attractionController.getAttractionList()) {
            for (Person person : ticket.getPersonList()) {
                if (person.isChildren()) {
                    attractionController.addRideChildrenToAttraction(ticket.getDate(), attraction, person.getHeight());
                    continue;
                }
                attractionController.addRideAdultToAttraction(ticket.getDate(), attraction, person.getHeight());
            }
        }
    }

    /**
     * Add Tickets and ride attraction for ticket persons
     *
     * @param ticketController
     * @param attractionController
     */
    public void addTicketsAndRide(TicketController ticketController, AttractionController attractionController) {
        TicketTypeController typeController = new TicketTypeController();

        /// Add GENERAL Tickets
        for (int i = 0; i < TICKET_GENERAL; i++) {
            Ticket ticket = this.getTicketByType(typeController, TicketTypeController.TICKET_GENERAL, (i % 3 == 0));
            ticketController.addTicket(ticket);
            this.addRideAttraction(ticket, attractionController);
        }

        /// Add FAMILY Tickets
        for (int i = 0; i < TICKET_FAMILY; i++) {
            Ticket ticket = this.getTicketFamily(typeController);
            ticketController.addTicket(ticket);
            this.addRideAttraction(ticket, attractionController);
        }

        /// Add AFTERNOON Tickets
        for (int i = 0; i < TICKET_AFTERNOON; i++) {
            Ticket ticket = this.getTicketByType(typeController, TicketTypeController.TICKET_AFTERNOON, (i % 5 == 0));
            ticketController.addTicket(ticket);
            this.addRideAttraction(ticket, attractionController);
        }

        /// Add LABORABLE Tickets
        int i = 0;
        do {
            Ticket ticket = this.getTicketByType(typeController, TicketTypeController.TICKET_LABORABLE, (i % 3 == 0));
            if (ticket.check(false)) {
                ticketController.addTicket(ticket);
                this.addRideAttraction(ticket, attractionController);
                i++;
            }
        } while (i < TICKECT_LABORABLE);
    }

    /**
     * Deactivate attraction
     *
     * @param controller
     * @param attraction
     */
    private void deactivateAttraction(AttractionController controller, Attraction attraction) {
        LocalDate date;
        for (int i = 0; i < ATTRACTION_MAX_DEACTIVATE; i++) {
            date = Utilities.randomDate(TEST_YEAR);
            controller.activateAttraction(attraction.getId(), date, date, false);
        }
    }

    /**
     * Return a family ticket with ramdon date
     *
     * @param typeController
     * @return Ticket
     */
    private Ticket getTicketFamily(TicketTypeController typeController) {
        /// Create ticket
        Ticket ticket = typeController.newTicketFromType(TicketTypeController.TICKET_FAMILY);

        /// Set date
        LocalDate date = Utilities.randomDate(TEST_YEAR);
        ticket.setDate(date);

        return ticket;
    }

    /**
     * Return a type indicate ticket with ramdon date and ramdon person number.
     *
     * @param typeController
     * @param type
     * @param vipSuplement
     * @return Ticket
     */
    private Ticket getTicketByType(TicketTypeController typeController, String type, boolean vipSuplement) {
        /// Create ticket
        Ticket ticket = typeController.newTicketFromType(type);

        /// Calculate num persons into the ticket
        int adults = Utilities.randomInt(1, TICKET_MAX_ADULTS) - 1; // The ticket always adds an adult
        int children = Utilities.randomInt(0, TICKET_MAX_CHILDREN);

        /// Set date
        LocalDate date = Utilities.randomDate(TEST_YEAR);
        ticket.setDate(date);

        /// Set firt adult
        this.configAdult(ticket, false, vipSuplement);

        /// Add more adults to ticket
        for (int j = 0; j < adults; j++) {
            this.configAdult(ticket, true, (j % 2 == 0));
        }

        /// Add children to ticket
        for (int j = 0; j < children; j++) {
            this.configChildren(ticket, vipSuplement);
        }

        return ticket;
    }

    /**
     * Configure or create a new adult person into ticket
     *
     * @param ticket
     * @param createPerson
     * @param vipSupplement
     */
    private void configAdult(Ticket ticket, boolean createPerson, boolean vipSupplement) {
        Person person;
        int age = Utilities.randomInt(PersonEnum.ADULT.getMinAge(), 99);
        int height = Utilities.randomInt(100, 200);

        if (createPerson) {
            int index = ticket.addPerson(age, height);
            person = ticket.getPerson(index);
        } else {
            person = ticket.getPerson(0);
            person.setAge(age);
        }

        person.setVipSupplement(vipSupplement);
    }

    /**
     * Add new infant person into ticket
     *
     * @param ticket
     * @param vipSupplement
     */
    private void configChildren(Ticket ticket, boolean vipSupplement) {
        int age = Utilities.randomInt(PersonEnum.CHILDREN.getMinAge(), PersonEnum.ADULT.getMinAge() - 1);
        int height = Utilities.randomInt(100, 200);
        int index = ticket.addPerson(age, height);
        Person person = ticket.getPerson(index);
        person.setVipSupplement(vipSupplement);
    }
}
