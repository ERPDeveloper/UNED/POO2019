/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package staff;

/**
 * Theme park application.
 * Base class for staff.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public abstract class StaffBase {

    /**
     * Set the base salary
     */
    private static final double BASE_SALARY = 950.00;

    /**
     * coefficient referring to the number of assistants in an attraction
     */
    protected byte coefficient;

    /**
     * Department name
     */
    protected String department;

    /**
     * Percentage supplement to apply
     */
    protected double supplement;

    /**
     * Constructor
     */
    public StaffBase() {
        this.coefficient = 100;
        this.department = "";
        this.supplement = 0.00;
    }

    /**
     * Get staff coefficient
     *
     * @return
     */
    public byte getCoefficient() {
        return this.coefficient;
    }

    /**
     * Get department name
     *
     * @return String
     */
    public String getDepartment() {
        return this.department;
    }

    /**
     * Returns the salary according base and supplement
     *
     * @return double
     */
    public double getSalary() {
        double salary = BASE_SALARY + (BASE_SALARY * this.supplement / 100.00);
        return Math.round(salary * 100.00) / 100.00;
    }

    /**
     * Get salary supplement
     *
     * @return String
     */
    public double getSupplement() {
        return this.supplement;
    }

    /**
     * Description formatted for printing
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Empleado tipo: " + this.department;
    }
}
