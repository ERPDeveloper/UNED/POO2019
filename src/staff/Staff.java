/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package staff;

import java.util.ArrayList;
import tools.Utilities;

/**
 * Classe to calculate personnel costs for an attraction
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class Staff {

    /**
     * Basic assistant of an attraction
     */
    private final StaffAssistantAttraction assistant;

    /**
     * Responsible for an attraction
     */
    private final StaffManagerAttraction manager;

    /**
     * list of staff dependent on the base staff number
     */
    private final ArrayList<StaffBase> staffList;

    /**
     * Contructor
     */
    public Staff() {
        this.assistant = new StaffAssistantAttraction();
        this.manager = new StaffManagerAttraction();

        this.staffList = new ArrayList<>();
        this.staffList.add(new StaffCustomerSupport());
        this.staffList.add(new StaffPublicRelation());
    }

    /**
     * Calculate the daily cost of the month according to the staff of an attraction
     *
     * @param year
     * @param month
     * @param numAssistant
     * @param numManager
     * @return double
     */
    public double dailyCost(int year, int month, int numAssistant, int numManager) {
        int days = Utilities.getMonthLength(year, month);
        double cost = this.monthlyCost(numAssistant, numManager);
        return Utilities.average(cost, days);
    }

    /**
     * Calculate the monthly cost according to the staff of an attraction
     *
     * @param numAssistant
     * @param numManager
     * @return double
     */
    public double monthlyCost(int numAssistant, int numManager) {
        // Calculate the number base staff (Assistant + Manager)
        int base1 = numAssistant * this.assistant.getCoefficient() / 100;
        int base2 = numManager * this.manager.getCoefficient() / 100;
        int total = base1 + base2;

        // Calculate base wage cost (Assistant + Manager)
        double cost = this.assistant.getSalary() * numAssistant;
        cost += this.manager.getSalary() * numManager;

        /// Add other staff wage cost.
        /// The Salary for the number of employees
        /// Calculate from employees base (assistant + manager) and applying employee coefficient.
        for (StaffBase staff : this.staffList) {
            cost += staff.getSalary() * Math.floor(total * staff.getCoefficient() / 100);
        }

        return cost;
    }
}
