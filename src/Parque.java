/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import tools.Menu;
import controller.AttractionController;
import controller.TicketController;
import controller.StatisticsController;

/**
 * Theme park application.
 * Management of attendance, attractions and staff.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class Parque {

    private static final String MENU_TEXT_CAPTION = "Menú Principal";

    /**
     * Main action. Application start.
     *
     * @param args
     */
    public static void main(String[] args) {
        /// Create main statistics controller
        StatisticsController statistics = new StatisticsController();

        /// Create main options controllers
        AttractionController attraction = new AttractionController();
        attraction.setStatistics(statistics);

        TicketController ticket = new TicketController();
        ticket.setStatistics(statistics);

        // Load test data
        Test test = new Test();
        test.addAttractions(attraction);
        test.addTicketsAndRide(ticket, attraction);

        // Create main menu
        Menu menu = new Menu();
        menu.addItem(ticket.getMenu());
        menu.addItem(attraction.getMenu());
        menu.addItem(statistics.getMenu());

        // Execute main menu
        menu.execute(MENU_TEXT_CAPTION, null);
    }
}
