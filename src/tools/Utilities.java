/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tools;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.TextStyle;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

/**
 * Tools Library.
 * Class with common or general utilities.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class Utilities {

    private static final String DATE_FORMATTER = "dd/MM/yyyy";

    private static final String ERROR_SCANNER_DATE = "¡Error! Introduce una fecha en el formato: " + DATE_FORMATTER;
    private static final String ERROR_SCANNER_INT = "¡Error! Debe introducir un valor numérico";
    private static final String ERROR_SCANNER_PERIOD = "¡Error! La fecha hasta es inferior a la desde";
    private static final String ERROR_SCANNER_STRING = "¡Error! El texto introducido es demasiado largo";

    private static final String ERROR_STRING = "**ERROR**";

    private static final String TEXT_SCANNER_DATE = "Introduzca la fecha con formato: " + DATE_FORMATTER;
    private static final String TEXT_SCANNER_DATE_FROM = "Introducir fecha (Desde):";
    private static final String TEXT_SCANNER_DATE_TO = "Introducir fecha (Hasta):";
    private static final String TEXT_SCANNER_PERIOD = "Indicar el periodo a listar";

    /**
     * Returns the average value for the indicated total
     *
     * @param total
     * @param times
     * @return double
     */
    public static double average(double total, int times) {
        return (times > 0)
            ? Math.round(total / times * 100.00) / 100.00
            : 0.00;
    }

    /**
     * Returns the average value for the indicated total
     *
     * @param total
     * @param times
     * @return int
     */
    public static int average(int total, int times) {
        return (times > 0) ? (total / times) : 0;
    }

    /**
     * Convert a Boolean value to Yes/No text
     *
     * @param value
     * @return String
     */
    public static String boolToString(boolean value) {
        return value ? "Sí" : "No";
    }

    /**
     * Convert a Date to String format
     *
     * @param date
     * @return String
     */
    public static String dateToString(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        return date.format(formatter);
    }

    /**
     * Returns a line string of the indicated length
     *
     * @param character
     * @param size
     * @return String
     */
    public static String getLine(char character, int size) {
        return new String(new char[size]).replace("\0", Character.toString(character));
    }

    /**
     * Return the number of days of a month
     *
     * @param year
     * @param month
     * @return int
     */
    public static int getMonthLength(int year, int month) {
        return YearMonth.of(year, Month.of(month)).lengthOfMonth();
    }

    /**
     * Returns the name of the indicated month
     *
     * @param month
     * @return String
     */
    public static String getMonthName(int month) {
        Locale locale = new Locale("es", "ES");
        return Month.of(month).getDisplayName(TextStyle.FULL, locale);
    }

    /**
     * Return the number of days of a year
     *
     * @param year
     * @return 365|366
     */
    public static int getYearLength(int year) {
        return Year.of(year).length();
    }

    /**
     * Check if a string date its a correct date
     *
     * @param date
     * @return boolean
     */
    public static boolean isValidDate(String date)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        try {
            LocalDate testDate = LocalDate.parse(date, formatter);
            return testDate.format(formatter).equals(date);
        } catch (DateTimeParseException e) {}
        return false;
    }

    /**
     * Indicates if the specified date is a weekend
     *
     * @param date
     * @return boolean
     */
    public static boolean isWeekend(LocalDate date) {
        switch (date.getDayOfWeek()) {
            case FRIDAY:
            case SATURDAY:
            case SUNDAY:
                return true;

            default:
                return false;
        }
    }

    /**
     * Get a random date for year indicated
     *
     * @param year
     * @return LocalDate
     */
    public static LocalDate randomDate(int year) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        int day, month;
        String date;

        do {
            day = randomInt(1, 31);
            month = randomInt(1, 12);
            date = String.format("%02d", day) + "/" + String.format("%02d", month) + "/" + String.format("%04d", year);
        } while (!isValidDate(date));

        return LocalDate.parse(date, formatter);
    }

    /**
     * Gets a random integer number in the range 1 to the maximum value indicated
     *
     * @param minValue
     * @param maxValue
     * @return int
     */
    public static int randomInt(int minValue, int maxValue) {
        Random aleatorio = new Random(System.currentTimeMillis());
        int value = aleatorio.nextInt(maxValue - minValue + 1);
        return value + minValue;
    }

    /**
     * Request a date value to the user. If indicated, an informative text is displayed.
     *
     * @param text
     * @return LocalDate
     */
    public static LocalDate requestDate(String text) {
        Scanner keyboard = new Scanner(System.in);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        String date = "";

        System.out.println(TEXT_SCANNER_DATE);
        do {
            if (!date.isEmpty()) {
                System.out.println(ERROR_SCANNER_DATE);
            }

            if (text.length() > 0) {
                System.out.print(text + " ");
            }

            date = keyboard.nextLine();
        } while (!date.isEmpty() && !Utilities.isValidDate(date));

        // Empty date
        if (date.isEmpty()) {
            return null;
        }

        // Return date introduced
        return LocalDate.parse(date, formatter);
    }

    /**
     * Request a numeric value to the user.If indicated, an informative text is displayed.
     *
     * @param text
     * @param minValue
     * @param maxValue
     * @return int
     */
    public static int requestInt(String text, int minValue, int maxValue) {
        Scanner keyboard = new Scanner(System.in);
        int result = 0;

        do {
            try {
                if (text.length() > 0) {
                    System.out.print(text + " ");
                }

                result = keyboard.nextInt();
                System.out.println("");
            } catch (Exception e) {
                keyboard.next();
                System.out.println(ERROR_SCANNER_INT);
                result = -1;
            }
        } while (
                result < 0
            || (result > maxValue && maxValue > 0)
            || (result > 0 && result < minValue && minValue > 0)
        );
        return result;
    }

    /**
     * Request a period of dates to the user. If indicated, an informative text is displayed.
     *
     * @param text
     * @return Period
     */
    public static HashMap<String, Object> requestPeriod(String text) {
        if (text.length() > 0) {
            System.out.print(text);
        } else {
            System.out.println(TEXT_SCANNER_PERIOD);
        }

        /// Request for dates
        LocalDate dateFrom = Utilities.requestDate(TEXT_SCANNER_DATE_FROM);
        if (dateFrom == null) {
            return null;
        }

        LocalDate dateTo = Utilities.requestDate(TEXT_SCANNER_DATE_TO);
        if (dateTo == null) {
            return null;
        }

        /// Check correcte period
        if (dateTo.isBefore(dateFrom)) {
            System.out.println(ERROR_SCANNER_PERIOD);
            return null;
        }

        /// Create result structure
        HashMap<String, Object> result = new HashMap<>();
        result.put("from", dateFrom);
        result.put("to", dateTo);
        result.put("period", Period.between(dateFrom, dateTo));

        /// Return introduced data
        return result;
    }

    /**
     * Request a string value to the user. If indicated, an informative text is displayed.
     *
     * @param text
     * @param maxLength
     * @return String
     */
    public static String requestString(String text, int maxLength) {
        Scanner keyborad = new Scanner(System.in);
        String result = "";

        do {
            try {
                if (text.length() > 0) {
                    System.out.print(text + " ");
                }

                result = keyborad.nextLine();
                System.out.println("");

                if (maxLength > 0 && result.length() > maxLength) {
                    System.out.println(ERROR_SCANNER_STRING);
                    result = ERROR_STRING;
                }
            } catch (Exception exception) {
                keyborad.next();
                System.out.println(ERROR_SCANNER_INT);
                result = ERROR_STRING;
            }
        } while (result.equals(ERROR_STRING));
        return result;
    }

    /**
     * Converts the first character of a string to uppercase.
     *
     * @param str
     * @return String
     */
    public static String ucFirst(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}
