/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tools;

import controller.Controller;
import java.util.ArrayList;

/**
 * Theme park application.
 * Each of the options of a menu.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class MenuItem {

    public static final int MENU_ITEM_CUSTOMEXIT = 99;

    /**
     * Text to visualize
     */
    private final String caption;

    /**
     * Next menu item level
     */
    private final ArrayList<MenuItem> childrenList;

    /**
     * Controller that will run
     */
    private final Controller controller;

    /**
     * Allows to establish a blank line before printing the menu option
     */
    private boolean marginTop;

    /**
     * Numeric option that the user must enter to select the option
     */
    private final int option;

    /**
     * Constructor
     *
     * @param caption
     * @param option
     * @param controller
     */
    public MenuItem(String caption, int option, Controller controller) {
        this.caption = caption;
        this.childrenList = new ArrayList<>();
        this.controller = controller;
        this.marginTop = false;
        this.option = option;
    }

    /**
     * Add an item to the menu
     *
     * @param caption
     * @param option
     * @return boolean
     */
    public MenuItem addChildren(String caption, int option) {
        MenuItem child = new MenuItem(caption, option, this.controller);
        this.childrenList.add(child);
        return child;
    }

    /**
     * Add an item to the menu with top margin
     *
     * @param caption
     * @param option
     * @param withMargin
     * @return boolean
     */
    public MenuItem addChildren(String caption, int option, boolean withMargin) {
        MenuItem item = this.addChildren(caption, option);
        item.setMarginTop(withMargin);
        return item;
    }

    /**
     * Get text to visualize
     *
     * @return String
     */
    public String getCaption() {
        if (this.controller == null) {
            return this.caption;
        }

        String optionCaption = this.controller.getOptionCaption(this);
        if (optionCaption.isEmpty()) {
            return this.caption;
        }
        return this.caption + "\t\t" + optionCaption;
    }

    /**
     * Get children menu items
     *
     * @return
     */
    public ArrayList<MenuItem> getChildrenList() {
        return childrenList;
    }

    /**
     * Get controller that will run
     *
     * @return Controller
     */
    public Controller getController() {
        return this.controller;
    }

    /**
     * Get option that the user must enter to select the option
     *
     * @return
     */
    public int getOption() {
        return this.option;
    }

    /**
     * Indicates if the item has children
     *
     * @return
     */
    public boolean hasChildren() {
        return this.childrenList.size() > 0;
    }

    /**
     * Returns if a previous margin is established
     *
     * @return boolean
     */
    public boolean hasMarginTop() {
        return marginTop;
    }

    /**
     * Set the previous margin
     *
     * @param marginTop
     */
    public void setMarginTop(boolean marginTop) {
        this.marginTop = marginTop;
    }
}
