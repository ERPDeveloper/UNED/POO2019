/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tools;

import java.util.ArrayList;

/**
 * Tools Library.
 * Class to manage a menu system.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class Menu {

    private static final int MENU_EXIT = 0;

    private static final String MENU_TEXT_EXIT = "Salir";
    private static final String MENU_TEXT_REQUEST = "Introducir la opción deseada:";
    private static final String MENU_TEXT_SEPARATOR = " - ";

    private String exitText;

    /**
     * List of menu options
     */
    private final ArrayList<MenuItem> optionList;

    /**
     * Constructor de la clase.
     */
    public Menu() {
        this.exitText = MENU_TEXT_EXIT;
        this.optionList = new ArrayList<>();
    }

    /**
     * Add an item to the menu
     *
     * @param menuItem
     * @return boolean
     */
    public boolean addItem(MenuItem menuItem) {
        return this.optionList.add(menuItem);
    }

    /**
     * It shows the menu and executes it until the user selects the exit option.
     *
     * @param title
     * @param items
     */
    public void execute(String title, ArrayList<MenuItem> items) {
        /// It's main menu?
        if (items == null) {
            items = this.optionList;
        }

        /// Main process
        int option;
        boolean repeat = true;
        do {
            /// Print menu and request option to user
            option = this.printAndRequest(title, items);

            if (option > 0) {
                MenuItem menuItem = items.get(option - 1);
                if (menuItem.hasChildren()) { /// Execute submenu items
                    this.execute(menuItem.getCaption(), menuItem.getChildrenList());
                    continue;
                }
                repeat = menuItem.getController().execOption(menuItem);
            }
        } while (option > 0 && repeat);
    }

    /**
     * Print menu options. Request to user for a valid option.
     *
     * @param title
     * @param items
     */
    private int printAndRequest(String title, ArrayList<MenuItem> items) {
        /// Print header menu
        System.out.println("");
        System.out.println(title);
        System.out.println(Utilities.getLine('=', title.length())); /// Print line of title size

        /// Print detail menu
        int index = 0;
        for (MenuItem item : items) {
            switch (item.getOption()) {
                case MenuItem.MENU_ITEM_CUSTOMEXIT:
                    this.exitText = item.getCaption();
                    break;

                default:
                    /// Print margin line
                    if (item.hasMarginTop()) {
                        System.out.println();
                    }
                    /// Print menu item
                    index++;
                    System.out.println(Integer.toString(index) + MENU_TEXT_SEPARATOR + item.getCaption());
                    break;
            }
        }

        System.out.println();
        System.out.println(Integer.toString(MENU_EXIT) + MENU_TEXT_SEPARATOR + this.exitText);
        System.out.println();
        return Utilities.requestInt(MENU_TEXT_REQUEST, 0, index);
    }
}
