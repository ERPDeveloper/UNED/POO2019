/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tools;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Tools Library.
 * Class to manage a holidays.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class Holiday {

    /**
     * Holidays list for current year
     */
    private final ArrayList<LocalDate> holidayList;

    /**
     * Constructor
     */
    public Holiday() {
        this.holidayList = new ArrayList<>();

        // Load holidays structure for current year
        int year = LocalDate.now().getYear();
        this.loadBasicHolidays(year);
        this.loadCustomHolidays(year);
    }

    /**
     * Indicates whether the specified date is a holiday
     *
     * @param date
     * @return boolean
     */
    public boolean isHoliday(LocalDate date) {
        for (LocalDate holiday : this.holidayList) {
            if (date.isEqual(holiday)) {
                return true;
            }
        }
        return false;
    }

    /**
     * List of common holidays
     *
     * @param year
     */
    private void loadBasicHolidays(int year) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        String yearStr = Integer.toString(year);

        this.holidayList.add( LocalDate.parse("01/01/" + yearStr, formatter) ); // Año nuevo
        this.holidayList.add( LocalDate.parse("01/05/" + yearStr, formatter) ); // Fiesta del Trabajo
        this.holidayList.add( LocalDate.parse("15/08/" + yearStr, formatter) ); // Asunción de María
        this.holidayList.add( LocalDate.parse("12/10/" + yearStr, formatter) ); // Hispanidad
        this.holidayList.add( LocalDate.parse("01/11/" + yearStr, formatter) ); // Todos los Santos
        this.holidayList.add( LocalDate.parse("06/12/" + yearStr, formatter) ); // Constitucion
        this.holidayList.add( LocalDate.parse("25/12/" + yearStr, formatter) ); // Natividad
    }

    /**
     * List of holidays of the community
     *
     * @param year
     */
    private void loadCustomHolidays(int year) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        String yearStr = Integer.toString(year);

        switch (year) {
            case 2019:
                this.holidayList.add( LocalDate.parse("20/01/" + yearStr, formatter) ); // San Sebastian
                this.holidayList.add( LocalDate.parse("01/03/" + yearStr, formatter) ); // Illes Balears
                this.holidayList.add( LocalDate.parse("18/04/" + yearStr, formatter) ); // Jueves Santo
                this.holidayList.add( LocalDate.parse("19/04/" + yearStr, formatter) ); // Viernes Santo
                this.holidayList.add( LocalDate.parse("22/04/" + yearStr, formatter) ); // Lunes de Pascua
                this.holidayList.add( LocalDate.parse("24/06/" + yearStr, formatter) ); // San Juan
                this.holidayList.add( LocalDate.parse("26/12/" + yearStr, formatter) ); // Segunda fiesta Natividad
                break;
        }
    }
}
