/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ticket;

/**
 * List of possible seasons and the supplement variation
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public enum SeasonEnum {

    MID_SEASON("Media", 0.00),
    HIGH_SEASON("Alta", 15.00),
    LOW_SEASON("Baja", -15.00);

    /**
     * Human description
     */
    private final String description;

    /**
     * Percentage supplement to apply
     */
    private final double supplement;

    /**
     * Constructor
     *
     * @param description
     * @param supplement
     */
    SeasonEnum(String description, double supplement) {
        this.description = description;
        this.supplement = supplement;
    }

    /**
     * Get the human description
     *
     * @return String
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Get the supplement percentage
     *
     * @return double
     */
    public double getSupplement() {
        return this.supplement;
    }

    /**
     * Description formatted for printing
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Temporada " + this.description;
    }
}
