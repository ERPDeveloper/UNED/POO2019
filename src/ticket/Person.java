/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ticket;

import java.util.ArrayList;

/**
 * Class to manage each one of the people of a ticket
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class Person {

    /*
     * Constants
     */
    private static final String TEXT_AGE = "Edad";
    private static final String TEXT_DISCOUNTS = "Descuentos";
    private static final String TEXT_HEIGHT = "Altura";
    private static final String TEXT_VIP_SUPPLEMENT = "Con Suplemento";

    /**
     * Set the price for the preferred pass or vip pass
     */
    static final double VIP_SUPPLEMENT_COST = 50.00;

    /**
     * Age of the person
     */
    private int age;

    /**
     * Additional list of discounts that apply to the group
     */
    private final ArrayList<DiscountEnum> discountList;

    /**
     * Group of the person. Set the base discount to apply
     */
    private PersonEnum group;

    /**
     * Height of the person
     */
    private int height;

    /**
     * Indicate if the group has a preferred pass or vip pass
     */
    private boolean vipSupplement;

    /**
     * Class constructor.Allows to build a new instance from the age
     *
     * @param age
     * @param height
     */
    public Person(int age, int height) {
        this.age = age;
        this.height = height;
        this.discountList = new ArrayList<>();
        this.group = PersonEnum.getPersonFromAge(age);
        this.vipSupplement = false;
    }

    /**
     * Class constructor.
     * Allows to build a new instance from the person group
     *
     * @param group
     */
    public Person(PersonEnum group) {
        this.age = group.getMinAge();
        this.height = group.getAverageHeight();
        this.discountList = new ArrayList<>();
        this.group = group;
        this.vipSupplement = false;
    }

    /**
     * Add a new discount to discount list
     *
     * @param newDiscount
     * @return boolean
     */
    public boolean addDiscount(DiscountEnum newDiscount) {
        // If the discount already exists in the list, do not add it
        for (DiscountEnum discount : this.discountList) {
            if (newDiscount == discount) {
                return false;
            }
        }
        // Add new discount to list
        return this.discountList.add(newDiscount);
    }

    /**
     * Simple description of age and group of person
     *
     * @return String
     */
    public String descriptionAge() {
        return TEXT_AGE + ": " + Integer.toString(this.age) + "   " + this.group.toString();
    }

    /**
     * Simple description of height and group of person
     *
     * @return String
     */
    public String descriptionHeight() {
        return TEXT_HEIGHT + ": " + Integer.toString(this.height);
    }

    /**
     * Descounts list display in line
     *
     * @return String
     */
    public String descriptionDescounts() {
        String text = TEXT_DISCOUNTS + ": ";
        String separator = "";
        for (DiscountEnum discount : this.discountList) {
            text = text + separator + discount.toString();
            separator = ", ";
        }
        return text;
    }

    /**
     * Simple description of vip supplement status
     *
     * @return String
     */
    public String descriptionVipSupplement() {
        return this.vipSupplement
                ? TEXT_VIP_SUPPLEMENT
                : "";
    }

    /**
     * Indicates the age of person
     *
     * @return int
     */
    public int getAge() {
        return this.age;
    }

    /**
     * Discounts list
     *
     * @return List of DiscountEnum
     */
    public ArrayList<DiscountEnum> getDiscounts() {
        return this.discountList;
    }

    /**
     * Indicates the type of the group
     *
     * @return PersonEnum
     */
    public PersonEnum getGroup() {
        return this.group;
    }

    /**
     * Indicates the height of person
     *
     * @return int
     */
    public int getHeight() {
        return height;
    }

    /**
     * Obtain price for person
     *
     * @param basePrice
     * @param minPrice
     * @return double
     */
    public double price(double basePrice, double minPrice) {
        /// Baby dont pay
        if (this.group == PersonEnum.BABY) {
            return 0.00;
        }

        /// Calculate price for person
        double price = basePrice - (basePrice * this.group.getDiscount() / 100.00);

        /// Calculate aditional discounts
        int totalDiscount = 0;
        for (DiscountEnum discount : this.discountList) {
            totalDiscount += discount.getDiscount();
        }
        price = price - (price * totalDiscount / 100);

        /// Set calculate price or min price, the greatest of them
        price = (price < minPrice) ? minPrice : price;

        /// Add vip supplement
        if (this.isVipSupplement()) {
            price += VIP_SUPPLEMENT_COST - (VIP_SUPPLEMENT_COST * this.group.getDiscount() / 100);
        }

        /// Return price
        return Math.round(price * 100.00) / 100.00;
    }

    /**
     * Indicates if person is a children
     *
     * @return boolean
     */
    public boolean isChildren() {
        return this.group.isChildren();
    }

    /**
     * Indicate if the group has a preferred pass or vip pass
     *
     * @return boolean
     */
    public boolean isVipSupplement() {
        return this.vipSupplement;
    }

    /**
     * Remove indicate discount from discount list
     *
     * @param index
     */
    public void removeDiscount(int index) {
        this.discountList.remove(index);
    }

    /**
     * Set the age and group for the person
     *
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
        this.group = PersonEnum.getPersonFromAge(age);
    }

    /**
     * Set the height for the person
     *
     * @param height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * Set or remove the preferred pass or vip pass for the group
     *
     * @param vipSupplement
     */
    public void setVipSupplement(boolean vipSupplement) {
        this.vipSupplement = vipSupplement;
    }

    /**
     * Description formatted for printing
     *
     * @return String
     */
    @Override
    public String toString() {
        String text = this.descriptionAge();
        if (this.vipSupplement) {
            text = text + " (" + TEXT_VIP_SUPPLEMENT + ")";
        }

        for (DiscountEnum discount : this.discountList) {
            text = text + "\n\t" + discount.toString();
        }
        return text;
    }
}
