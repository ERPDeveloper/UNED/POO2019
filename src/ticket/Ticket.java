/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ticket;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Main class for ticket types for access to park
 * 
 * @author Jose Antonio Cuello Principal
 * @version 1.0
 * @since 01 mar 2019
 */
public abstract class Ticket {

    /**
     * Constants for error messages
     */
    private static final String ERROR_NO_ADULTS = "¡Error! Debe haber al menos un Adulto";

    /**
     * Consts
     */
    private static final String TEXT_ADULTS = "Adultos";
    private static final String TEXT_CHILDREN = "Niños";
    private static final String TEXT_TICKET = "Entrada";

    /**
     * Set the base price for a ticket
     */
    private static final double BASE_PRICE = 60.00;

    /**
     * Set the percentage for the calculation of the minimum price of the ticket
     */
    private static final double MIN_PCT_PRICE = 10.00;

    /**
     * Unique identifier counter
     */
    private static int nextIDTicket = 1;

    /**
     * Total number of adults on the ticket
     */
    private int adults;

    /**
     * Total number of children on the ticket
     */
    private int children;

    /**
     * Date of the tickets for access to the park
     */
    private LocalDate date;

    /**
     * Human description
     */
    protected String description;

    /**
     * Percentage discount applied
     */
    protected double discount;

    /**
     * Unique id for ticket
     */
    private int id;

    /**
     * List of people included in the entry
     * Each group of persons can have assigned discounts and supplements
     */
    private final ArrayList<Person> personList;

    /**
     * Selected season
     */
    private final Season season;

    /**
     * Place of origin from which the ticket was issued
     */
    private SourceEnum source;

    /**
     * Constructor
     */
    public Ticket() {
        /// Assign the current unique id and update the counter
        this.id = Ticket.nextIDTicket++;

        /// Initialize with the default data
        this.adults = 0;
        this.children = 0;
        this.date = LocalDate.now();
        this.description = "";
        this.discount = 0.00;
        this.source = SourceEnum.OFFICE_SOURCE;

        /// Set the season for the date
        this.season = new Season();
        this.season.setSeasonDate(this.date);

        /// Create person structure and add one
        this.personList = new ArrayList<>();
        this.addPerson(PersonEnum.ADULT);
    }

    /**
     * Add a new person to the ticket according to age
     *
     * @param age
     * @param height
     * @return int
     */
    public final int addPerson(int age, int height) {
        Person person = new Person(age, height);
        return this.addPerson(person);
    }

    /**
     * Add a new person to the ticket according to the group
     *
     * @param group
     * @return int
     */
    public final int addPerson(PersonEnum group) {
        Person person = new Person(group);
        return this.addPerson(person);
    }

    /**
     * Add a new person to the ticket
     *
     * @param person
     * @return
     */
    private int addPerson(Person person) {
        if (this.personList.add(person)) {
            if (person.isChildren()) {
                this.children++;
            } else {
                this.adults++;
            }
            return this.personList.size() - 1;
        }
        return -1;
    }

   /**
     * Returns the base price according to the season
     *
     * @return double
     */
    private double basePrice() {
        double supplement = this.season.getSupplement();
        double price = BASE_PRICE + (BASE_PRICE * supplement / 100.00);
        return Math.round(price * 100.00) / 100.00;
    }

    /**
     * Returns the base price according to the type of entry and the season
     *
     * @return double
     */
    private double baseTicketPrice() {
        double basePrice = this.basePrice();
        double price = basePrice - (basePrice * this.discount / 100.00);
        return Math.round(price * 100.00) / 100.00;
    }

    /**
     * Check if the group meets all the requirements
     *
     * @param showMessage
     * @return boolean
     */
    public boolean check(boolean showMessage) {
        if (this.adults < 1) {
            if (showMessage) {
                System.out.println(ERROR_NO_ADULTS);
            }
            return false;
        }
        return true;
    }

    /**
     * Copy custom attributes into current ticket from param ticket
     *
     * @param ticket
     */
    public void copyFromTicket(Ticket ticket) {
        this.adults = ticket.adults;
        this.children = ticket.children;
        this.date = ticket.date;
        this.id = ticket.id;
        this.source = ticket.source;

        this.personList.clear();
        this.personList.addAll(ticket.personList);

        this.season.setSeasonDate(this.date);
    }

    /**
     * Simple description of persons groups
     *
     * @return String
     */
    public String descriptionPerson() {
        return TEXT_ADULTS + ": " + Integer.toString(this.adults) + "   "
            + TEXT_CHILDREN + ": " + Integer.toString(this.children);
    }

    /**
     * Simple description of total price
     * @return
     */
    public String descriptionPrice() {
        return "Total: " + Double.toString(this.total()) + "€";
    }

    /**
     * Simple description of selected season
     *
     * @return String
     */
    public String descriptionSeason() {
        return this.season.toString();
    }

    /**
     * Total number of adults
     *
     * @return int
     */
    public int getAdults() {
        return this.adults;
    }

    /**
     * Total number of children
     *
     * @return int
     */
    public int getChildren() {
        return this.children;
    }

    /**
     * Indicates date of the tickets for access to the park
     *
     * @return
     */
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * Simple description of the type of entry
     *
     * @return String
     */
    public String getDescription() {
        return this.description;
    }

   /**
     * Get id for ticket
     *
     * @return int
     */
    public int getId() {
        return this.id;
    }

    /**
     * Obtain the person from the list in the indicated position
     *
     * @param index
     * @return
     */
    public Person getPerson(int index) {
        return this.personList.get(index);
    }

    /**
     * Get person list
     *
     * @return List of Person
     */
    public ArrayList<Person> getPersonList() {
        return this.personList;
    }

   /**
     * Get id with source serie for ticket
     *
     * @return String
     */
    public String getSerieId() {
        return this.source.getSerie() + Integer.toString(this.id);
    }

    /**
     * Returns the min price for a ticket (one person)
     *
     * @return double
     */
    public double minPrice() {
        double price = this.basePrice() * MIN_PCT_PRICE / 100.00;
        return Math.round(price * 100.00) / 100.00;
    }

    /**
     * Get quantities for adults and children
     *
     * @return int[]
     */
    public int[] quantityByType() {
        return new int[] {this.adults, this.children};
    }

    /**
     * Recalculates the number and types of people included in the ticket
     */
    public void recalculatePersons() {
        this.adults = 0;
        this.children = 0;
        for (Person person : this.personList) {
            if (person.isChildren()) {
                this.children++;
                continue;
            }
            this.adults++;
        }
    }

    /**
     * Remove the indicated person from the list
     *
     * @param index
     */
    public void removePerson(int index) {
        Person person = this.personList.remove(index);
        if (person.isChildren()) {
            this.children--;
            return;
        }
        this.adults--;
    }

    /**
     * Set ticket entry date
     * Establishes the season according to the informed date
     *
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
        this.season.setSeasonDate(date);
    }

    /**
     * Set Place of origin from which the ticket was issued
     *
     * @param source
     */
    public void setSource(SourceEnum source) {
        this.source = source;
    }

    /**
     * Description formatted for printing
     *
     * @return String
     */
    @Override
    public String toString() {
        return TEXT_TICKET + " " + this.description + ": " + this.getId() + " (" + this.season.toString() + ")";
    }

    /**
     * Get the total price of the ticket
     *
     * @return
     */
    public double total() {
        double basePrice = this.baseTicketPrice();
        double minPrice = this.minPrice();

        double amount = 0.00;
        for (Person person : this.personList) {
            amount += person.price(basePrice, minPrice);
        }
        return amount;
    }

    /**
     * Get total price for adults and children
     *
     * @return double[]
     */
    public double[] totalByType() {
        double basePrice = this.baseTicketPrice();
        double minPrice = this.minPrice();

        double amountAdults = 0.00;
        double amountChildren = 0.00;
        for (Person person : this.personList) {
            if (person.isChildren()) {
                amountChildren += person.price(basePrice, minPrice);
                continue;
            }
            amountAdults += person.price(basePrice, minPrice);
        }
        return new double[] { amountAdults, amountChildren };
    }
}
