/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ticket;

import java.time.LocalDate;
import tools.Holiday;
import tools.Utilities;

/**
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class TicketLaborable extends Ticket {

    /*
     * Constants for error messages
     */
    private static final String ERROR_NO_LABORABLE = "¡Error! La fecha del ticket no es laborable";

    /**
     * Class responsible for controlling the holiday calendar
     */
    private final Holiday holiday;

    /**
     * Constructor
     */
    public TicketLaborable() {
        super();

        this.discount = 20.00;
        this.description = "Día Laborable";
        this.holiday = new Holiday();
    }

    /**
     * Check if the group meets all the requirements
     *
     * @return boolean
     */
    @Override
    public boolean check(boolean showMessage) {
        LocalDate date = this.getDate();
        if (this.holiday.isHoliday(date) || Utilities.isWeekend(date)) {
            if (showMessage) {
                System.out.println(ERROR_NO_LABORABLE);
            }
            return false;
        }
        return super.check(showMessage);
    }
}
