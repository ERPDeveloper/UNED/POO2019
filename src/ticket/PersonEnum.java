/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ticket;

/**
 * List of types of groups that can be sold.
 * Set the base discount to apply to the base price.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public enum PersonEnum {

    ADULT("Adulto", 12, 165, 0),
    BABY("Bebe", 0, 30, 100),
    CHILDREN("Niño", 3, 120, 50),
    SENIOR("Senior", 65, 160, 35);

    /**
     * Human description
     */
    private final String description;

    /**
     * Percentage discount applied
     */
    private final int discount;

    /**
     * Minimum age of the person
     */
    private final int minAge;

    /**
     * Average height of the person
     */
    private final int averageHeight;

    /**
     * Constructor
     *
     * @param description
     * @param discount
     */
    private PersonEnum(String description, int minAge, int height, int discount) {
        this.averageHeight = height;
        this.description = description;
        this.discount = discount;
        this.minAge = minAge;
    }

    /**
     * It indicates whether the group is infantile
     *
     * @return boolean
     */
    public boolean isChildren() {
        return (this == CHILDREN || this == BABY);
    }

    /**
     * Get the average height for group
     * @return
     */
    public int getAverageHeight() {
        return this.averageHeight;
    }

    /**
     * Get the human description
     *
     * @return String
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Get the discount percentage
     *
     * @return int
     */
    public int getDiscount() {
        return this.discount;
    }

    /**
     * Get the minimum age for group
     *
     * @return int
     */
    public int getMinAge() {
        return this.minAge;
    }

    /**
     * Returns the type of group for a person according to their age
     *
     * @param age
     * @return PersonEnum
     */
    public static PersonEnum getPersonFromAge(int age) {
        if (age < PersonEnum.CHILDREN.minAge) {
            return PersonEnum.BABY;
        }

        if (age < PersonEnum.ADULT.minAge) {
            return PersonEnum.CHILDREN;
        }

        if (age < PersonEnum.SENIOR.minAge) {
            return PersonEnum.ADULT;
        }

        return PersonEnum.SENIOR;
    }

    /**
     * Description formatted for printing
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Grupo: " + this.description;
    }
}
