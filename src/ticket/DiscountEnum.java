/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ticket;

import java.util.ArrayList;
import java.util.EnumSet;

/**
 * List of possible discounts
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public enum DiscountEnum {

    STUDENT("Estudiante", 10),
    YOUTH_CARD("Carnet Joven", 10),
    UNEMPLOYED("Desempleado", 10),
    FUNCTIONAL_DIVERSITY("Diversidad Funcional", 20);

    /**
     * Human description
     */
    private final String description;

    /**
     * Percentage discount applied
     */
    private final int discount;

    /**
     * Constructor
     *
     * @param description
     * @param discount
     */
    DiscountEnum(String description, int discount) {
        this.description = description;
        this.discount = discount;
    }

    /**
     * Get the list of possible discounts
     *
     * @return List of DiscountEnum
     */
    public static ArrayList<DiscountEnum> discountList() {
        return new ArrayList<>(EnumSet.allOf(DiscountEnum.class));
    }

    /**
     * Get the human description
     *
     * @return String
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Get the discount percentage
     *
     * @return int
     */
    public int getDiscount() {
        return this.discount;
    }

    /**
     * Description formatted for printing
     *
     * @return String
     */
    @Override
    public String toString() {
        return this.description + "(" + this.discount + "%)";
    }
}
