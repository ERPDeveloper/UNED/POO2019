/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ticket;

/**
 * List of possible origins of ticket sales
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public enum SourceEnum {

    OFFICE_SOURCE("Taquilla", "TK"),
    ONLINE_SOURCE("Venta Online", "ON");

    /**
     * Human description
     */
    private final String description;

    /**
     * Id for source
     */
    private final String serie;

    /**
     * Constructor
     *
     * @param description
     */
    private SourceEnum(String description, String serie) {
        this.description = description;
        this.serie = serie;
    }

    /**
     * Get the human description
     *
     * @return String
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Get the id for source
     *
     * @return String
     */
    public String getSerie() {
        return serie;
    }

    /**
     * Description formatted for printing
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Venta en " + this.description;
    }
}
