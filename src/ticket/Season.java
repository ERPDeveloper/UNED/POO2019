/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ticket;

import java.time.LocalDate;

/**
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class Season {

    /**
     * Store the Season
     */
    private SeasonEnum season;

    /**
     * Constructor
     */
    public Season() {
        this.season = SeasonEnum.MID_SEASON;
    }

    /**
     * Get the supplement percentage
     *
     * @return double
     */
    public double getSupplement() {
        return this.season.getSupplement();
    }

    /**
     * Establishes the season according to the informed date
     *
     * @param date
     */
    public void setSeasonDate(LocalDate date) {

        switch (date.getMonthValue()) {
            case 4:
            case 8:
            case 12:
                this.season = SeasonEnum.HIGH_SEASON;
                break;

            case 2:
            case 11:
                this.season = SeasonEnum.LOW_SEASON;
                break;

            default:
                if ((date.getMonthValue() == 1) && (date.getDayOfMonth() <= 8)) {
                    this.season = SeasonEnum.HIGH_SEASON;
                } else {
                    this.season = SeasonEnum.MID_SEASON;
                }
                break;
        }
    }

    /**
     * Description formatted for printing
     *
     * @return String
     */
    @Override
    public String toString() {
        return this.season.toString();
    }
}
