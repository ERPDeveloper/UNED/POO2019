/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ticket;

/**
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class TicketFamily extends Ticket {

    private static final int MIN_ADULTS = 2;
    private static final int MIN_CHILDREN = 2;

    /*
     * Constants for error messages
     */
    private static final String ERROR_NO_ADULTS = "¡Error! Debe haber al menos " + Integer.toString(MIN_ADULTS) + " Adultos";
    private static final String ERROR_NO_CHILDREN = "¡Error! Debe haber al menos " + Integer.toString(MIN_CHILDREN) + " Niños";

    /**
     * Constructor
     */
    public TicketFamily() {
        super();

        this.discount = 20.00;
        this.description = "Familiar";

        /// Add one adult more and two children
        this.addPerson(PersonEnum.ADULT);
        this.addPerson(PersonEnum.CHILDREN);
        this.addPerson(PersonEnum.CHILDREN);

        /// Add VIP Pass
        for (Person person : this.getPersonList()) {
            person.setVipSupplement(true);
        }
    }

    /**
     * Check if the group meets all the requirements
     *
     * @return boolean
     */
    @Override
    public boolean check(boolean showMessage) {
        if (this.getAdults() < MIN_ADULTS) {
            if (showMessage) {
                System.out.println(ERROR_NO_ADULTS);
            }
            return false;
        }

        if (this.getChildren() < MIN_CHILDREN) {
            if (showMessage) {
                System.out.println(ERROR_NO_CHILDREN);
            }
            return false;
        }
        return super.check(showMessage);
    }

    /**
     * Copy custom attributes into current ticket from param ticket
     *
     * @param ticket
     */
    @Override
    public void copyFromTicket(Ticket ticket) {
        super.copyFromTicket(ticket);

        /// Create min adults
        int adults = MIN_ADULTS - this.getAdults();
        if (adults > 0) {
            for (int i = 0; i < adults; i++) {
                this.addPerson(PersonEnum.ADULT);
            }
        }

        /// Create min children
        int children = MIN_CHILDREN - this.getChildren();
        if (children > 0) {
            for (int i = 0; i < children; i++) {
                this.addPerson(PersonEnum.CHILDREN);
            }
        }

        /// Add VIP Pass
        for (Person person : this.getPersonList()) {
            person.setVipSupplement(true);
        }
    }
}
