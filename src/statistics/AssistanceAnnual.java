/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package statistics;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Theme park application.
 * Main class for annual assistance data management
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class AssistanceAnnual {

    /**
     * Year of cumulative statistics
     */
    private final int year;

    /**
     * Array of assistance data accumulated by months.
     * The zero element contains the annual cumulative
     */
    private final ArrayList<AssistancePerson> monthsList;

    /**
     * Array of assistance data accumulated per day.
     */
    private final ArrayList<AssistancePerson> daysList;

    /**
     * Constructor
     *
     * @param year
     */
    public AssistanceAnnual(int year) {
        this.daysList = new ArrayList<>();
        this.monthsList = new ArrayList<>();
        this.year = year;

        /// Create records data for year and mounths
        for (int i = 0; i < 13; i++) {
            this.monthsList.add(new AssistancePerson());
        }

        /// Create records data for days
        for (int i = 0; i < 366; i++) {
            this.daysList.add(new AssistancePerson());
        }
    }

    /**
     * Accumulate the quantity and amount on the indicated day and month
     *
     * @param date
     * @param quantity
     * @param amount
     * @param children
     */
    private void accumulate(LocalDate date, int quantity, double amount, boolean children) {
        if (date.getYear() != this.year) {
            return;
        }
        int day = date.getDayOfYear() - 1;
        int month = date.getMonthValue();

        AssistancePerson dataDay = this.daysList.get(day);
        AssistancePerson dataMonth = this.monthsList.get(month);
        AssistancePerson dataYear = this.monthsList.get(0);

        /// Accumulate Children, if its a children
        if (children) {
            dataDay.accumulateChildren(quantity, amount);
            dataMonth.accumulateChildren(quantity, amount);
            dataYear.accumulateChildren(quantity, amount);
            return;
        }

        /// Accumulate Adults
        dataDay.accumulateAdults(quantity, amount);
        dataMonth.accumulateAdults(quantity, amount);
        dataYear.accumulateAdults(quantity, amount);
    }

    /**
     * Accumulate the quantity and amount on the indicated day and month
     *
     * @param date
     * @param quantity
     * @param amount
     */
    public void accumulateAdults(LocalDate date, int quantity, double amount) {
        this.accumulate(date, quantity, amount, false);
    }

    /**
     * Accumulate the quantity and amount on the indicated day and month
     *
     * @param date
     * @param quantity
     * @param amount
     */
    public void accumulateChildren(LocalDate date, int quantity, double amount) {
        this.accumulate(date, quantity, amount, true);
    }

    /**
     * Obtain data of the indicated day of year
     *
     * @param dayOfYear
     * @return AssistancePerson
     */
    public AssistancePerson getAssistanceDay(int dayOfYear) {
        if (dayOfYear < 1) {
            dayOfYear = 1;
        }
        return this.daysList.get(dayOfYear - 1);
    }

    /**
     * Obtain data of the indicated month
     *
     * @param month
     * @return AssistancePerson
     */
    public AssistancePerson getAssistanceMonth(int month) {
        if (month < 1 || month > 12) {
            return new AssistancePerson();
        }
        return this.monthsList.get(month);
    }

    /**
     * Obtains the accumulated data of the year
     *
     * @return AssistancePerson
     */
    public AssistancePerson getAssistanceYear() {
        return this.monthsList.get(0);
    }

    /**
     * Year of the statistics
     *
     * @return int
     */
    public int getYear() {
        return this.year;
    }
}
