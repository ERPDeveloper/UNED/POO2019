/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package statistics;

import java.time.LocalDate;
import java.util.HashMap;
import tools.Utilities;

/**
 * Class for report the assistance statistics of the park
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public abstract class StatisticsReport {

    /*
     * Constants general
     */
    private static final int MIN_YEAR = 2015;

    /*
     * Constants for messages/labels
     */
    protected static final String TEXT_ADULTS = "Adultos";
    protected static final String TEXT_CHILDREN = "Niños";
    protected static final String TEXT_NUM_DAYS = "Número días";
    protected static final String TEXT_TOTAL = "Total";

    /*
     * Constants for request
     */
    protected static final String TEXT_MONTH = "Introducir el mes:";
    protected static final String TEXT_YEAR = "Introducir el año:";

    /*
     * Constants for error messages
     */
    private static final String ERROR_PERIOD_YEAR = "¡Error! El periodo debe estar dentro del mismo año";
    private static final String ERROR_YEAR = "¡Error! Debe introducir un año superior a " + (MIN_YEAR - 1);

    /**
     * Displays the annual statistics
     */
    abstract public void annual();

    /**
     * Displays the statistics of a date period
     */
    abstract public void byDate();

    /**
     * Displays the monthly statistics
     */
    abstract public void monthly();

    /**
     * Constructor
     */
    public StatisticsReport() {
    }

    /**
     * Requests the user one period higher than year MIN_YEAR
     *
     * @return int
     */
    protected HashMap<String, LocalDate> requestPeriod() {
        HashMap<String, Object> period = Utilities.requestPeriod("");
        if (period == null) {
            return null;
        }

        /// Get and check date from year
        LocalDate dateFrom = (LocalDate) period.get("from");
        int year = dateFrom.getYear();
        if (year < MIN_YEAR) {
            System.out.println(ERROR_YEAR);
            return null;
        }

        /// Get and check date to period
        LocalDate dateTo = (LocalDate) period.get("to");
        if (year != dateTo.getYear()) {
            System.out.println(ERROR_PERIOD_YEAR);
            return null;
        }

        /// Return data
        HashMap<String, LocalDate> result = new HashMap<>();
        result.put("from", dateFrom);
        result.put("to", dateTo);
        return result;
    }

    /**
     * Requests the user one year higher than MIN_YEAR
     *
     * @return int
     */
    protected int requestYear() {
        int year = Utilities.requestInt(TEXT_YEAR, MIN_YEAR, LocalDate.now().getYear());
        if (year > 0 && year < MIN_YEAR) {
            System.out.println(ERROR_YEAR);
            return 0;
        }

        return year;
    }
}
