/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package statistics;

import java.util.Formatter;

/**
 * Theme park application.
 * Class for the storage of statistical assistance data.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class AssistanceData {

    private final String TEXT_FORMAT_TOSTRING = "Cantidad: %,6d\tImporte: %,10.2f€\tMedia: %,5.2f€";

    /**
     * Accumulated amount
     */
    private double amount;

    /**
     * Average price according to the amount and the accumulated amount
     */
    private double average;

    /**
     * Accumulated quantity
     */
    private int quantity;

    /**
     * Constructor
     */
    public AssistanceData() {
        this.amount = 0.00;
        this.average = 0.00;
        this.quantity = 0;
    }

    /**
     * Accumulate/Deacumulate the indicated amount
     *
     * @param amount
     */
    public void accumulateAmount(double amount) {
        this.amount += amount;
        if (this.amount < 0) {
            this.amount = 0;
        }
        this.calculateAverage();
    }

    /**
     * Accumulate/Deacumulate the indicated quantity
     *
     * @param quantity
     */
    public void accumulateQuantity(int quantity) {
        this.quantity += quantity;
        if (this.quantity < 0) {
            this.quantity = 0;
        }
        this.calculateAverage();
    }

    /**
     * Calculate the average based on the amount and the accumulated amount
     */
    private void calculateAverage() {
        this.average = (this.quantity == 0) ? 0.00 : this.amount / this.quantity;
    }

    /**
     * Obtain the total amount accumulated
     *
     * @return double
     */
    public double getAmount() {
        return this.amount;
    }

    /**
     * Get the average
     *
     * @return double
     */
    public double getAverage() {
        return this.average;
    }

    /**
     * Obtain the total quantity accumulated
     *
     * @return int
     */
    public int getQuantity() {
        return this.quantity;
    }


    /**
     * Description formatted for printing
     *
     * @return String
     */
    @Override
    public String toString() {
        Formatter formatter = new Formatter();
        formatter.format(TEXT_FORMAT_TOSTRING, this.quantity, this.amount, this.average);
        return formatter.toString();
    }
}
