/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package statistics;

import java.time.LocalDate;
import java.util.HashMap;
import tools.Utilities;

/**
 * Class for report the assistance statistics of the park
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class AssistanceReport extends StatisticsReport {

    /*
     * Constants for messages/labels
     */
    private static final String TEXT_ASSISTANCE = "Asistencia";
    private static final String TEXT_MEDIA_AMOUNT = "Recaudación media diaria";
    private static final String TEXT_MEDIA_QUANTITY = "Entrada media diaria";

    /**
     * Assistance statistics
     */
    private final AssistanceStatistics assistance;

    /**
     * Constructor
     *
     * @param assistance
     */
    public AssistanceReport(AssistanceStatistics assistance) {
        super();
        this.assistance = assistance;
    }

    /**
     * Displays attendance data for one year
     */
    @Override
    public void annual() {
        int year = this.requestYear();
        if (year == 0) {
            return;
        }

        AssistancePerson data = this.assistance.getStatisticForYear(year);
        String title = TEXT_ASSISTANCE + ": " + Integer.toString(year);
        this.print(title, data, Utilities.getYearLength(year));
    }

    /**
     * Displays attendance data between a period of dates
     */
    @Override
    public void byDate() {
        /// Request period to list
        HashMap<String, LocalDate> period = this.requestPeriod();
        if (period == null) {
            return;
        }

        LocalDate dateFrom = period.get("from");
        LocalDate dateTo = period.get("to");
        int year = dateTo.getYear();

        /// Main process. Accumulate period selected
        int startDay = dateFrom.getDayOfYear();
        int endDay = dateTo.getDayOfYear() + 1;
        AssistancePerson data = new AssistancePerson();
        for (int i = startDay; i < endDay; i++) {
            AssistancePerson assistanceDay = this.assistance.getStatisticForDay(year, i);
            data.accumulateAdults(assistanceDay.getAdults());
            data.accumulateChildren(assistanceDay.getChildren());
        }

        /// Print data
        String title = TEXT_ASSISTANCE + ": " + dateFrom.toString() + " - " + dateTo.toString();
        this.print(title, data, (endDay - startDay));
    }

    /**
     * Displays attendance data for one month
     */
    @Override
    public void monthly() {
        int year = this.requestYear();
        if (year == 0) {
            return;
        }

        int month = Utilities.requestInt(TEXT_MONTH, 0, 12);
        if (month == 0) {
            return;
        }

        AssistancePerson data = this.assistance.getStatisticForMonth(year, month);
        String monthName = Utilities.getMonthName(month).toUpperCase();
        String title = TEXT_ASSISTANCE + ": " + monthName + " / " + Integer.toString(year);
        this.print(title, data, Utilities.getMonthLength(year, month));
    }

    /**
     * Show attendance data on screen
     *
     * @param title
     * @param data
     * @param numDays
     */
    private void print(String title, AssistancePerson data, int numDays) {
        /// Calculate daily media to print
        AssistanceData total = data.getTotal();
        int quantity = total.getQuantity() / numDays;
        double amount = Utilities.average(total.getAmount(), numDays);

        /// Print data
        System.out.println(title);
        System.out.println(Utilities.getLine('-', title.length()));

        System.out.println(TEXT_NUM_DAYS + ": " + Integer.toString(numDays));
        System.out.println(TEXT_MEDIA_QUANTITY + ": " + Integer.toString(quantity));
        System.out.println(TEXT_MEDIA_AMOUNT + ": " + Double.toString(amount) + "€");
        System.out.println("");
        System.out.println(TEXT_ADULTS + " -> " + data.getAdults().toString());
        System.out.println(TEXT_CHILDREN + "   -> " + data.getChildren().toString());
        System.out.println(TEXT_TOTAL + "   -> " + total.toString());
        System.out.println("");
    }
}
