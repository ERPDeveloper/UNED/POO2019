/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal <yopli2000@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package statistics;

import attractions.Attraction;
import java.time.LocalDate;
import java.util.HashMap;
import tools.Utilities;

/**
 * Class for report the ride attraction statistics of the park
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class RideAttractionReport extends StatisticsReport {

    /*
     * Constants for request
     */
    private static final String TEXT_ID_ATTRACTION = "Introducir el Id. de la atracción:";

    /*
     * Constants for messages/labels
     */
    private static final String TEXT_RIDE_ATTRACTION = "Uso de Atracción";
    private static final String TEXT_ACTIVE_DAYS = "Días Activa";
    private static final String TEXT_MEDIA = "Media";

    /*
     * Constants for error messages
     */
    private static final String ERROR_ATTRACTION = "¡Error! No existe la atracción";

    /**
     * Assistance statistics
     */
    private final RideAttractionStatistics rideAttraction;

    /**
     * Constructor
     *
     * @param rideAttraction
     */
    public RideAttractionReport(RideAttractionStatistics rideAttraction) {
        super();
        this.rideAttraction = rideAttraction;
    }

    /**
     * Displays ride attraction data for one year
     */
    @Override
    public void annual() {
        /// Ask the user for the necessary data
        int year = this.requestYear();
        if (year == 0) {
            return;
        }

        String id = this.requestIdAttraction();
        if (id.isEmpty()) {
            return;
        }

        RideAttractionAnnual annual = this.rideAttraction.searchStatisticForYear(year, id);
        if (annual == null) {
            System.out.println(ERROR_ATTRACTION);
            return;
        }

        /// Obtain all data for printing
        Attraction attraction = annual.getAttraction();
        RideAttraction data = annual.getRideAttractionYear();
        int activeDays = annual.getAttractionActiveForYear();
        String title = TEXT_RIDE_ATTRACTION + ": " + Integer.toString(year);
        String cost = attraction.descriptionAnnualCost();

        /// Show statistics
        this.print(title, cost, attraction, data, activeDays, Utilities.getYearLength(year));
    }

    /**
     * Displays ride attraction data between a period of dates
     */
    @Override
    public void byDate() {
        /// Request period to list
        HashMap<String, LocalDate> period = this.requestPeriod();
        if (period == null) {
            return;
        }

        String id = this.requestIdAttraction();
        if (id.isEmpty()) {
            return;
        }

        LocalDate dateFrom = period.get("from");
        LocalDate dateTo = period.get("to");
        int year = dateTo.getYear();
        int month = dateTo.getMonthValue();

        RideAttractionAnnual annual = this.rideAttraction.searchStatisticForYear(year, id);
        if (annual == null) {
            System.out.println(ERROR_ATTRACTION);
            return;
        }

        /// Obtain all data for printing
        Attraction attraction = annual.getAttraction();
        int activeDays = annual.getAttractionActiveForPeriod(dateFrom, dateTo);
        String title = TEXT_RIDE_ATTRACTION + ": " + dateFrom.toString() + " - " + dateTo.toString();
        String cost = attraction.descriptionPeriodCost(year, month, activeDays);

        /// Accumulate period selected
        int startDay = dateFrom.getDayOfYear();
        int endDay = dateTo.getDayOfYear() + 1;

        RideAttraction data = new RideAttraction();
        for (int i = startDay; i < endDay; i++) {
            RideAttraction rideAttractionDay = annual.getRideAttractionDay(i);
            data.accumulateAdults(rideAttractionDay.getAdults());
            data.accumulateChildren(rideAttractionDay.getChildren());
        }

        /// Show statistics
        this.print(title, cost, attraction, data, activeDays, (endDay - startDay));
    }

    /**
     * Displays ride attraction data for one month
     */
    @Override
    public void monthly() {
        /// Ask the user for the necessary data
        int year = this.requestYear();
        if (year == 0) {
            return;
        }

        int month = Utilities.requestInt(TEXT_MONTH, 0, 12);
        if (month == 0) {
            return;
        }

        String id = this.requestIdAttraction();
        if (id.isEmpty()) {
            return;
        }

        RideAttractionAnnual annual = this.rideAttraction.searchStatisticForYear(year, id);
        if (annual == null) {
            System.out.println(ERROR_ATTRACTION);
            return;
        }

        /// Obtain all data for printing
        Attraction attraction = annual.getAttraction();
        RideAttraction data = annual.getRideAttractionMonth(month);
        int activeDays = annual.getAttractionActiveForMonth(month);
        String monthName = Utilities.getMonthName(month).toUpperCase();
        String title = TEXT_RIDE_ATTRACTION + ": " + monthName + " / " + Integer.toString(year);
        String cost = attraction.descriptionMonthlyCost();

        /// Show statistics
        this.print(title, cost, attraction, data, activeDays, Utilities.getMonthLength(year, month));
    }

    /**
     * Show ride attraction data on screen
     *
     * @param title
     * @param cost
     * @param attraction
     * @param data
     * @param activeDays
     * @param numDays
     */
    private void print(
            String title,
            String cost,
            Attraction attraction,
            RideAttraction data,
            int activeDays,
            int numDays
    ) {
        /// Print data
        System.out.println(title);
        System.out.println(Utilities.getLine('-', title.length()));

        System.out.println(attraction.toString());
        System.out.println("");
        System.out.println(TEXT_NUM_DAYS + ": " + Integer.toString(numDays));
        System.out.println(TEXT_ACTIVE_DAYS + ": " + Integer.toString(activeDays));
        System.out.println(cost);
        System.out.println("");

        int adults = data.getAdults();
        if (adults > 0) {
            int average = Utilities.average(adults, activeDays);
            System.out.printf("%s -> %,d\t%s -> %,d\n", TEXT_ADULTS, adults, TEXT_MEDIA, average);
        }

        int children = data.getChildren();
        if (children > 0) {
            int average = Utilities.average(children, activeDays);
            System.out.printf("%s   -> %,d\t%s -> %,d\n", TEXT_CHILDREN, children, TEXT_MEDIA, average);
        }

        if (attraction.isAllowsAdult() && attraction.isAllowsChildren()) {
            int average = Utilities.average(data.getTotal(), activeDays);
            System.out.printf("%s   -> %,d\t%s -> %,d\n", TEXT_TOTAL, data.getTotal(), TEXT_MEDIA, average);
        }
        System.out.println("");
    }

    /**
     * Requests the user one string id attraction
     *
     * @return int
     */
    private String requestIdAttraction() {
        return Utilities.requestString(TEXT_ID_ATTRACTION, 0);
    }
}
