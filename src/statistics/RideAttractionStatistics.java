/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package statistics;

import attractions.Attraction;
import attractions.AttractionStatus;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Theme park application.
 * Main class for ride attraction statistics
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class RideAttractionStatistics {

    /**
     * List of annuals statistics
     */
    private final ArrayList<RideAttractionAnnual> rideAttractionList;

    /**
     * Constructor
     */
    public RideAttractionStatistics() {
        this.rideAttractionList = new ArrayList<>();
    }

    /**
     * Accumulate the quantities indicated and according to the date,
     * in the year, month and day.The quantities is an array: int Adults, int Children.
     *
     * @param date
     * @param attraction
     * @param status
     * @param quantities
     */
    public void accumulateIntoYear(LocalDate date, Attraction attraction, AttractionStatus status, int[] quantities) {
        /// Search for statistic year record
        int year = date.getYear();
        RideAttractionAnnual statistic = this.searchStatisticForYear(year, attraction.getId());
        if (statistic == null) {
            // Dont exits, create a new record for year
            statistic = new RideAttractionAnnual(year, attraction, status);
            this.rideAttractionList.add(statistic);
        }

        /// Acumulate into statistic
        statistic.accumulateAdults(date, quantities[0]);
        statistic.accumulateChildren(date, quantities[1]);
    }

    /**
     * Get the number of active days of the attraction for the year indicated
     *
     * @param year
     * @param id
     * @return int
     */
    public int getAttractionActiveForYear(int year, String id) {
        RideAttractionAnnual statistic = this.searchStatisticForYear(year, id);
        return statistic == null ? 0 : statistic.getAttractionActiveForYear();
    }

    /**
     * Get the number of active days of the attraction for the year and month indicated
     *
     * @param year
     * @param id
     * @param month
     * @return int
     */
    public int getAttractionActiveForMonth(int year, String id, int month) {
        RideAttractionAnnual statistic = this.searchStatisticForYear(year, id);
        return statistic == null ? 0 : statistic.getAttractionActiveForMonth(month);
    }

    /**
     * Obtains the statistic for indicated month
     *
     * @param year
     * @param id
     * @param month
     * @return RideAttraction
     */
    public RideAttraction getStatisticForMonth(int year, String id, int month) {
        RideAttractionAnnual statistic = this.searchStatisticForYear(year, id);
        return statistic == null ? null : statistic.getRideAttractionMonth(month);
    }

    /**
     * Obtains the statistic for indicated year
     *
     * @param year
     * @param id
     * @return RideAttraction
     */
    public RideAttraction getStatisticForYear(int year, String id) {
        RideAttractionAnnual statistic = this.searchStatisticForYear(year, id);
        return statistic == null ? null : statistic.getRideAttractionYear();
    }

    /**
     * Search and return the statistics record for the indicated attraction and year
     *
     * @param year
     * @param id
     * @return AssistanceAnnual
     */
    public RideAttractionAnnual searchStatisticForYear(int year, String id) {
        for (RideAttractionAnnual statistic : this.rideAttractionList) {
            if (statistic.getYear() == year && statistic.getId().equals(id)) {
                return statistic;
            }
        }
        return null;
    }
}
