/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package statistics;

/**
 * Theme park application.
 * Class for the storage of statistical ride attraction data by group of people
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class RideAttraction {

    /**
     * Adult statistical data
     */
    private int adults;

    /**
     * Infant statistical data
     */
    private int children;

    /**
     * Total (Adult + Infant) statistical data
     */
    private int total;

    /**
     * Constructor
     */
    public RideAttraction() {
        this.adults = 0;
        this.children = 0;
        this.total = 0;
    }

    /**
     * Accumulates the data reported in the group of adults
     *
     * @param quantity
     */
    public void accumulateAdults(int quantity) {
        this.adults += quantity;
        this.accumulateTotal(quantity);
    }

    /**
     * Accumulates the data reported in the group of infants
     *
     * @param quantity
     */
    public void accumulateChildren(int quantity) {
        this.children += quantity;
        this.accumulateTotal(quantity);
    }

    /**
     * Accumulates the data reported in the total
     *
     * @param quantity
     */
    private void accumulateTotal(int quantity) {
        this.total += quantity;
    }

    /**
     * Obtain number adults that ride
     *
     * @return int
     */
    public int getAdults() {
        return this.adults;
    }

    /**
     * Obtain number infants that ride
     *
     * @return int
     */
    public int getChildren() {
        return this.children;
    }

    /**
     * Obtain total number that ride
     *
     * @return int
     */
    public int getTotal() {
        return this.total;
    }
}
