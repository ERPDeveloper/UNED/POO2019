/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package statistics;

/**
 * Theme park application.
 * Class for the storage of statistical assistance data by group of people
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class AssistancePerson {

    /**
     * Adult statistical data
     */
    private final AssistanceData adults;

    /**
     * Infant statistical data
     */
    private final AssistanceData children;

    /**
     * Total (Adult + Infant) statistical data
     */
    private final AssistanceData total;

    /**
     * Constructor
     */
    public AssistancePerson() {
        this.adults = new AssistanceData();
        this.children = new AssistanceData();
        this.total = new AssistanceData();
    }

    /**
     * Accumulates the data reported in the group of adults
     *
     * @param quantity
     * @param amount
     */
    public void accumulateAdults(int quantity, double amount) {
        this.adults.accumulateQuantity(quantity);
        this.adults.accumulateAmount(amount);

        this.accumulateTotal(quantity, amount);
    }

    /**
     * Accumulates the data reported in the group of adults
     *
     * @param data
     */
    public void accumulateAdults(AssistanceData data) {
        this.accumulateAdults(data.getQuantity(), data.getAmount());
    }

    /**
     * Accumulates the data reported in the group of infants
     *
     * @param quantity
     * @param amount
     */
    public void accumulateChildren(int quantity, double amount) {
        this.children.accumulateQuantity(quantity);
        this.children.accumulateAmount(amount);

        this.accumulateTotal(quantity, amount);
    }

    /**
     * Accumulates the data reported in the group of infants
     *
     * @param data
     */
    public void accumulateChildren(AssistanceData data) {
        this.accumulateChildren(data.getQuantity(), data.getAmount());
    }

    /**
     * Accumulates the data reported in the total
     *
     * @param quantity
     * @param amount
     */
    private void accumulateTotal(int quantity, double amount) {
        this.total.accumulateQuantity(quantity);
        this.total.accumulateAmount(amount);
    }

    /**
     * Obtain the assistance data of adults
     *
     * @return AssistanceData
     */
    public AssistanceData getAdults() {
        return this.adults;
    }

    /**
     * Obtain the assistance data of the infants
     *
     * @return AssistanceData
     */
    public AssistanceData getChildren() {
        return this.children;
    }

    /**
     * Obtain the total assistance data
     *
     * @return AssistanceData
     */
    public AssistanceData getTotal() {
        return this.total;
    }


}
