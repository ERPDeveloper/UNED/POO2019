/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package statistics;

import attractions.Attraction;
import attractions.AttractionStatus;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Theme park application.
 * Main class for annual ride attraction data management
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class RideAttractionAnnual {

    /**
     * Link to attraction
     */
    private final Attraction attraction;

    /**
     * Link to attraction status
     */
    private final AttractionStatus status;

    /**
     * Year of cumulative statistics
     */
    private final int year;

    /**
     * Array of ride attraction data accumulated by months.
     * The zero element contains the annual cumulative
     */
    private final ArrayList<RideAttraction> monthsList;

    /**
     * Array of ride attraction data accumulated per day.
     */
    private final ArrayList<RideAttraction> daysList;

    /**
     * Constructor
     *
     * @param year
     * @param attraction
     * @param status
     */
    public RideAttractionAnnual(int year, Attraction attraction, AttractionStatus status) {
        this.daysList = new ArrayList<>();
        this.monthsList = new ArrayList<>();
        this.year = year;
        this.attraction = attraction;
        this.status = status;

        /// Create records data for year and mounths
        for (int i = 0; i < 13; i++) {
            this.monthsList.add(new RideAttraction());
        }

        /// Create records data for days
        for (int i = 0; i < 366; i++) {
            this.daysList.add(new RideAttraction());
        }
    }

    /**
     * Accumulate the quantity on the indicated day and month
     *
     * @param date
     * @param quantity
     * @param children
     */
    private void accumulate(LocalDate date, int quantity, boolean children) {
        if (date.getYear() != this.year) {
            return;
        }
        int day = date.getDayOfYear() - 1;
        int month = date.getMonthValue();

        RideAttraction dataDay = this.daysList.get(day);
        RideAttraction dataMonth = this.monthsList.get(month);
        RideAttraction dataYear = this.monthsList.get(0);

        /// Accumulate Children, if its a children
        if (children) {
            dataDay.accumulateChildren(quantity);
            dataMonth.accumulateChildren(quantity);
            dataYear.accumulateChildren(quantity);
            return;
        }

        /// Accumulate Adults
        dataDay.accumulateAdults(quantity);
        dataMonth.accumulateAdults(quantity);
        dataYear.accumulateAdults(quantity);
    }

    /**
     * Accumulate the quantity on the indicated day and month
     *
     * @param date
     * @param quantity
     */
    public void accumulateAdults(LocalDate date, int quantity) {
        this.accumulate(date, quantity, false);
    }

    /**
     * Accumulate the quantity on the indicated day and month
     *
     * @param date
     * @param quantity
     */
    public void accumulateChildren(LocalDate date, int quantity) {
        this.accumulate(date, quantity, true);
    }

    /**
     * Obtain data of the indicated month
     *
     * @param month
     * @return RideAttraction
     */
    public RideAttraction getRideAttractionMonth(int month) {
        if (month < 1 || month > 12) {
            return new RideAttraction();
        }
        return this.monthsList.get(month);
    }

    /**
     * Obtain data of the indicated day of year
     *
     * @param dayOfYear
     * @return RideAttraction
     */
    public RideAttraction getRideAttractionDay(int dayOfYear) {
        if (dayOfYear < 1) {
            dayOfYear = 1;
        }
        return this.daysList.get(dayOfYear - 1);
    }

    /**
     * Obtains the accumulated data of the year
     *
     * @return RideAttraction
     */
    public RideAttraction getRideAttractionYear() {
        return this.monthsList.get(0);
    }

    /**
     * Attraction of the statistics
     *
     * @return Attraction
     */
    public Attraction getAttraction() {
        return this.attraction;
    }

    /**
     * Get the number of active days of the attraction for the year and month indicated
     *
     * @param month
     * @return int
     */
    public int getAttractionActiveForMonth(int month) {
        return this.status.getActiveMonth(month);
    }

    /**
     * Get the number of active days of the attraction for the period indicated
     *
     * @param dateFrom
     * @param dateTo
     * @return int
     */
    public int getAttractionActiveForPeriod(LocalDate dateFrom, LocalDate dateTo) {
        int startDay = dateFrom.getDayOfYear();
        int endDay = dateTo.getDayOfYear() + 1;
        int active = 0;

        for (int i = startDay; i < endDay; i++) {
            if (this.status.getStatus(i)) {
                active++;
            }
        }

        return active;
    }

    /**
     * Get the number of active days of the attraction for the year indicated
     *
     * @return int
     */
    public int getAttractionActiveForYear() {
        return this.status.getActiveYear();
    }

    /**
     * Get attraction id
     *
     * @return String
     */
    public String getId() {
        return this.attraction.getId();
    }

    /**
     * Year of the statistics
     *
     * @return int
     */
    public int getYear() {
        return this.year;
    }
}
