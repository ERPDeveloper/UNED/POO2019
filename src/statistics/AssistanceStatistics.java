/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package statistics;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Theme park application.
 * Main class for assistance statistics
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class AssistanceStatistics {

    /**
     * List of annuals statistics
     */
    private final ArrayList<AssistanceAnnual> assistanceList;

    /**
     * Constructor
     */
    public AssistanceStatistics() {
        this.assistanceList = new ArrayList<>();
    }

    /**
     * Accumulate the quantities and amounts indicated and according to the date,
     * in the year, month and day.
     *
     * @param date
     * @param quantities
     * @param amounts
     */
    public void accumulateIntoYear(LocalDate date, int[] quantities, double[] amounts) {
        /// Search for statistic year record
        int year = date.getYear();
        AssistanceAnnual statistic = this.searchStatisticForYear(year);
        if (statistic == null) {
            // Dont exits, create a new record for year
            statistic = new AssistanceAnnual(year);
            this.assistanceList.add(statistic);
        }

        /// Acumulate into statistic
        statistic.accumulateAdults(date, quantities[0], amounts[0]);
        statistic.accumulateChildren(date, quantities[1], amounts[1]);
    }

    /**
     * Accumulate the quantities and amounts indicated and according to the date,
     * in the year, month and day.
     *
     * @param date
     * @param quantities
     * @param amounts
     */
    public void desacumulateIntoYear(LocalDate date, int[] quantities, double[] amounts) {
        /// Search for statistic year record
        int year = date.getYear();
        AssistanceAnnual statistic = this.searchStatisticForYear(year);
        if (statistic == null) {
            return;
        }

        /// Deacumulate from statistic
        statistic.accumulateAdults(date, (quantities[0] * -1), (amounts[0] * -1));
        statistic.accumulateChildren(date, (quantities[1] * -1), (amounts[1] * -1));
    }

    /**
     * Obtains the statistic for indicated day of year
     *
     * @param year
     * @param dayOfYear
     * @return AssistancePerson
     */
    public AssistancePerson getStatisticForDay(int year, int dayOfYear) {
        return this.searchStatisticForYear(year).getAssistanceDay(dayOfYear);
    }

    /**
     * Obtains the statistic for indicated month
     *
     * @param year
     * @param month
     * @return AssistancePerson
     */
    public AssistancePerson getStatisticForMonth(int year, int month) {
        return this.searchStatisticForYear(year).getAssistanceMonth(month);
    }

    /**
     * Obtains the statistic for indicated year
     *
     * @param year
     * @return AssistancePerson
     */
    public AssistancePerson getStatisticForYear(int year) {
        return this.searchStatisticForYear(year).getAssistanceYear();
    }

    /**
     * Search and return the statistics record for the indicated year
     *
     * @param year
     * @return AssistanceAnnual
     */
    private AssistanceAnnual searchStatisticForYear(int year) {
        for (AssistanceAnnual statisticAnnual : this.assistanceList) {
            if (statisticAnnual.getYear() == year) {
                return statisticAnnual;
            }
        }
        return null;
    }
}
