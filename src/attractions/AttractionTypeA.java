/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package attractions;

/**
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class AttractionTypeA extends Attraction {

    private static final String ATTRACTION_TYPE = "A";

    /**
     * Constructor
     *
     * @param id
     */
    public AttractionTypeA(String id) {
        super(id);

        this.minimunHeight = 120;
        this.numAssistant = 6;
    }

    /**
     * Calculate a unique identifier for the attraction
     *
     * @param id
     * @return String
     */
    @Override
    protected String calculateID(String id) {
        return ATTRACTION_TYPE + id;
    }
}
