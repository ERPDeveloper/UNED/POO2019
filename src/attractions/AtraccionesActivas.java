/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package attractions;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Theme park application.
 * Class to control the state of park attractions.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class AtraccionesActivas {

    /**
     * List of attractions states
     */
    private final ArrayList<AttractionStatus> attractionStatusList;

    /**
     * Constructor
     */
    public AtraccionesActivas() {
        this.attractionStatusList = new ArrayList<>();
    }

    /**
     * Add new attraction control status for indicated year
     *
     * @param year
     * @param id
     */
    public void addAttraction(int year, String id) {
        if (this.getAttractionStatus(year, id) == null) {
            AttractionStatus status = new AttractionStatus(year, id);
            this.attractionStatusList.add(status);
        }
    }

    /**
     * Change the state of an attraction for the indicated date period
     * The date period must belong to the same year
     *
     * @param id
     * @param dateFrom
     * @param dateTo
     * @param active
     */
    public void changeAttractionStatus(String id, LocalDate dateFrom, LocalDate dateTo, boolean active) {
        int year = dateFrom.getYear();
        if (year != dateTo.getYear()) {
            return;
        }

        AttractionStatus attractionStatus = this.getAttractionStatus(year, id);
        if (attractionStatus == null) {
            return;
        }

        attractionStatus.setStatus(dateFrom, dateTo, active);
    }

    /**
     * Check if the state control record of the attraction corresponds
     * to the attraction and year indicated
     *
     * @param attractionStatus
     * @param year
     * @param id
     * @return boolean
     */
    private boolean checkAttraction(AttractionStatus attractionStatus, int year, String id) {
        return attractionStatus.getYear() == year && attractionStatus.getId().equals(id);
    }

    /**
     * Get the status data for the indicated year and attraction
     *
     * @param year
     * @param id
     * @return AttractionStatus|null
     */
    public AttractionStatus getAttractionStatus(int year, String id) {
        for (AttractionStatus attractionStatus : this.attractionStatusList) {
            if (this.checkAttraction(attractionStatus, year, id)) {
                return attractionStatus;
            }
        }
        return null;
    }

    /**
     * Remove attraction status control for one or all years
     * (year == 0 [all years])
     *
     * @param year
     * @param id
     */
    public void removeAttraction(int year, String id) {
        Iterator<AttractionStatus> it = this.attractionStatusList.iterator();
        while (it.hasNext()) {
            AttractionStatus item = it.next();
            if (item.getId().equals(id)) {
                if (item.getYear() == year || year == 0) {
                    it.remove();
                }
            }
        }
    }
}
