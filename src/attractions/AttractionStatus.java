/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package attractions;

import java.time.LocalDate;
import tools.Utilities;

/**
 * Theme park application.
 * Class that indicates the annual activity status of an attraction.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class AttractionStatus {

    /**
     * Active number of days per month.
     * The zero element indicates the number of active days for the year.
     */
    private final int[] active;

    /**
     * Attraction identifier
     */
    private final String id;

    /**
     * Indicator of the state of attraction
     */
    private final boolean[] status;

    /**
     * Indicate the year
     */
    private final int year;

    /**
     * Constructor
     *
     * @param year
     * @param id
     */
    public AttractionStatus(int year, String id) {
        this.id = id;
        this.year = year;

        /// Init to true al year status
        int yearLength = Utilities.getYearLength(this.year);
        this.status = new boolean[yearLength];
        for (int i = 0; i < this.status.length; i++) {
            this.status[i] = true;
        }

        /// Init active days counts
        this.active = new int[13];
        this.active[0] = yearLength;
        for (int i = 1; i < this.active.length; i++) {
            this.active[i] = Utilities.getMonthLength(year, i);
        }
    }

    /**
     * adds or subtracts a day of activity to the month and year
     * according to the indicated state.
     *
     * @param month
     * @param add
     */
    private void accumulateActiveDay(int month, boolean add) {
        int value = add ? 1 : -1;

        this.active[0] += value;
        this.active[month] += value;
    }

    /**
     * Get the number of days that the actraction has been active
     * during the indicated month.
     *
     * @param month
     * @return int
     */
    public int getActiveMonth(int month) {
        if (month <1 || month > 12) {
            return 0;
        }
        return this.active[month];
    }

    /**
     * Gets the number of days that the actraction has been active during the year
     *
     * @return int
     */
    public int getActiveYear() {
        return this.active[0];
    }

    /**
     * Obtain the attraction id
     *
     * @return String
     */
    public String getId() {
        return id;
    }

    /**
     * Obtain the state of attraction for the day of the year indicated
     *
     * @param dayOfYear
     * @return boolean
     */
    public boolean getStatus(int dayOfYear) {
        return dayOfYear < 1
            ? this.status[0]
            : this.status[dayOfYear - 1];
    }

    /**
     * Get the status of the attraction for the indicated date
     *
     * @param date
     * @return boolean
     */
    public boolean getStatus(LocalDate date) {
        return this.getStatus(date.getDayOfYear());
    }

    /**
     * Obtain the year
     *
     * @return int
     */
    public int getYear() {
        return year;
    }

    /**
     * Establishes the state for the day of the indicated year
     *
     * @param dayOfYear
     * @param newStatus
     */
    public void setStatus(int dayOfYear, boolean newStatus) {
        if (dayOfYear > 0) {
            boolean oldStatus = this.status[dayOfYear - 1];
            if (oldStatus != newStatus) {
                /// Set new status
                this.status[dayOfYear - 1] = newStatus;

                /// Recalculate active days
                LocalDate date = LocalDate.ofYearDay(year, dayOfYear);
                this.accumulateActiveDay(date.getMonthValue(), newStatus);
            }
        }
    }

    /**
     * Set the status for the indicated date
     *
     * @param date
     * @param newStatus
     */
    public void setStatus(LocalDate date, boolean newStatus) {
        this.setStatus(date.getDayOfYear(), newStatus);
    }

    /**
     * Set the status for the indicated period
     *
     * @param dateFrom
     * @param dateTo
     * @param newStatus
     */
    public void setStatus(LocalDate dateFrom, LocalDate dateTo, boolean newStatus) {
        if (dateFrom.getYear() != dateTo.getYear()) {
            return;
        }

        for (int i = dateFrom.getDayOfYear(); i < dateTo.getDayOfYear() + 1; i++) {
            this.setStatus(i, newStatus);
        }
    }
}
