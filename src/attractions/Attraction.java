/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package attractions;

import java.util.Formatter;
import staff.Staff;
import tools.Utilities;


/**
 * Theme park application.
 * Base class for attractions.
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public abstract class Attraction {

    /**
     * Consts
     */
    private static final String TEXT_ATTRACTION = "Atracción";
    private static final String TEXT_ATTRACTION_ANNUAL_COST = "Coste anual";
    private static final String TEXT_ATTRACTION_AVERAGE_COST = "Coste medio";
    private static final String TEXT_ATTRACTION_DAILY_COST = "Coste diario";
    private static final String TEXT_ATTRACTION_MONTHLY_COST = "Coste mensual";
    private static final String TEXT_ATTRACTION_PERIOD_COST = "Coste del periodo";
    private static final String TEXT_FORMAT_TOSTRING = "Asistentes: %d\tEncargados: %d\nAdultos: %s\tNiños: %s\tPase VIP: %s\nAltura Máx.: %d\nAltura Mín.: %d";

    /**
     * Indicates if the attraction allows adults
     */
    protected boolean allowsAdult;

    /**
     * Indicates if the attraction allows children
     */
    protected boolean allowsChildren;

    /**
     * Indicates if the attraction allows vip pass
     */
    protected boolean allowsVipPass;

    /**
     * Unique identificator for attraction
     */
    protected String id;

    /**
     * Indicates the maximum height of the user of the attraction
     */
    protected int maximumHeight;

    /**
     * Indicates the minimun height of the user of the attraction
     */
    protected int minimunHeight;

    /**
     * Indicates the number of assistants for the maintenance of the attraction
     */
    protected byte numAssistant;

    /**
     * Indicates the number of managers for the maintenance of the attraction
     */
    protected byte numManager;

    /**
     * Calculate a unique identifier for the attraction
     *
     * @param id
     * @return String
     */
    protected abstract String calculateID(String id);

    /**
     * Constructor
     *
     * @param id
     */
    public Attraction(String id) {
        this.allowsAdult = true;
        this.allowsChildren = true;
        this.allowsVipPass = true;

        this.maximumHeight = 999;
        this.minimunHeight = 0;

        this.numAssistant = 1;
        this.numManager = 1;

        this.id = this.calculateID(id);
    }

    /**
     * Check if the characteristics of a person are suitable to ride on the attraction
     *
     * @param height
     * @param isChildren
     * @return boolean
     */
    public boolean check(int height, boolean isChildren) {
        if (isChildren == true && this.allowsChildren == false) {
            return false;
        }

        if (isChildren == false && this.allowsAdult == false) {
            return false;
        }

        return (height > this.minimunHeight) && (height <= this.maximumHeight);
    }

    /**
     * Obtain the annual cost of attraction staff
     *
     * @return double
     */
    public double annualCost() {
        return this.monthlyCost() * 12;
    }

    /**
     * Obtain the daily cost of attraction staff
     *
     * @param year
     * @param month
     * @return double
     */
    public double dailyCost(int year, int month) {
        Staff staff = new Staff();
        return staff.dailyCost(year, month, this.numAssistant, this.numManager);
    }

    /**
     * Returns the descriptive text for users about the annual cost of the staff
     *
     * @return String
     */
    public String descriptionAnnualCost() {
        double cost = this.annualCost();
        return this.formatDescriptionCost(TEXT_ATTRACTION_ANNUAL_COST, cost);
    }

    /**
     * Returns the descriptive text for users about the daily cost of the staff
     *
     * @param year
     * @param month
     * @return String
     */
    public String descriptionDailyCost(int year, int month) {
        double cost = this.dailyCost(year, month);
        return this.formatDescriptionCost(TEXT_ATTRACTION_DAILY_COST, cost);
    }

    /**
     * Returns the descriptive text for users about the period cost of the staff
     *
     * @param year
     * @param month
     * @param numDays
     * @return String
     */
    public String descriptionPeriodCost(int year, int month, int numDays) {
        double cost = this.dailyCost(year, month) * numDays;
        double average = Utilities.average(cost, numDays);
        String period = this.formatDescriptionCost(TEXT_ATTRACTION_PERIOD_COST, cost);
        String daily = this.formatDescriptionCost(TEXT_ATTRACTION_AVERAGE_COST, average);
        return period + "\t" + daily;
    }

    /**
     * Returns the descriptive text for users about the monthly cost of the staff
     *
     * @return String
     */
    public String descriptionMonthlyCost() {
        double cost = this.monthlyCost();
        return this.formatDescriptionCost(TEXT_ATTRACTION_MONTHLY_COST, cost);
    }

    /**
     * Format a description of a cost
     *
     * @param text
     * @param cost
     * @return String
     */
    private String formatDescriptionCost(String text, double cost) {
        Formatter formatter = new Formatter();
        formatter.format("%-,9.2f€", cost);
        return text + ": " + formatter.toString();
    }

    /**
     * Obtain the monthly cost of attraction staff
     *
     * @return double
     */
    public double monthlyCost() {
        Staff staff = new Staff();
        return staff.monthlyCost(this.numAssistant, this.numManager);
    }

    /**
     * Get attraction unique identificator
     *
     * @return
     */
    public String getId() {
        return this.id;
    }

    /**
     * Indicates if the attraction allows adults
     *
     * @return boolean
     */
    public boolean isAllowsAdult() {
        return this.allowsAdult;
    }

    /**
     * Indicates if the attraction allows children
     *
     * @return boolean
     */
    public boolean isAllowsChildren() {
        return this.allowsChildren;
    }

    /**
     * Indicates if the attraction allows vip pass
     *
     * @return boolean
     */
    public boolean isAllowsVipPass() {
        return this.allowsVipPass;
    }

    /**
     * Get the maximum height in cm
     *
     * @return int
     */
    public int getMaximumHeight() {
        return this.maximumHeight;
    }

    /**
     * Get the minimun height in cm
     *
     * @return int
     */
    public int getMinimunHeight() {
        return this.minimunHeight;
    }

    /**
     * Get the number of assistant
     *
     * @return byte
     */
    public byte getNumAssistant() {
        return this.numAssistant;
    }

    /**
     * Get the number of managers
     *
     * @return byte
     */
    public byte getNumManager() {
        return this.numManager;
    }

    /**
     * Description formatted for printing
     *
     * @return String
     */
    @Override
    public String toString() {
        Formatter formatter = new Formatter();
        formatter.format(TEXT_FORMAT_TOSTRING,
                this.numAssistant,
                this.numManager,
                Utilities.boolToString(this.allowsAdult),
                Utilities.boolToString(this.allowsChildren),
                Utilities.boolToString(this.allowsVipPass),
                this.maximumHeight,
                this.minimunHeight
        );

        String text = TEXT_ATTRACTION + ": " + this.id + "\n";
        text += Utilities.getLine('-', text.length()) + "\n";
        text += formatter.toString();
        return text;
    }
}
