/*
 * Copyright (C) 2019 Jose Antonio Cuello Principal 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package attractions;

/**
 *
 * @author Jose Antonio Cuello Principal 
 * @version 1.0 - 01 mar 2019
 */
public class AttractionTypeB extends Attraction {

    private static final String ATTRACTION_TYPE = "B";

    /**
     * Constructor
     *
     * @param id
     */
    public AttractionTypeB(String id) {
        super(id);

        this.allowsChildren = false;
        this.allowsVipPass = false;
        this.maximumHeight = 190;
        this.minimunHeight = 120;
        this.numAssistant = 5;
    }

    /**
     * Calculate a unique identifier for the attraction
     *
     * @param id
     * @return String
     */
    @Override
    protected String calculateID(String id) {
        return ATTRACTION_TYPE + id;
    }
}
